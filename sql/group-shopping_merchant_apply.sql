-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: group-shopping
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `merchant_apply`
--

DROP TABLE IF EXISTS `merchant_apply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `merchant_apply` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '数据主键',
  `create_time` datetime NOT NULL COMMENT '数据创建时间',
  `update_time` datetime NOT NULL COMMENT '数据更新时间',
  `short_name` varchar(45) NOT NULL COMMENT '简称',
  `full_name` varchar(100) NOT NULL COMMENT '全称',
  `legal_person` varchar(45) NOT NULL COMMENT '法人代表',
  `certificate_no` varchar(45) NOT NULL COMMENT '证件号',
  `license_no` varchar(45) NOT NULL COMMENT '执照编号',
  `phone` varchar(45) NOT NULL COMMENT '手机号',
  `telephone` varchar(45) DEFAULT NULL COMMENT '电话',
  `email` varchar(45) NOT NULL COMMENT '邮箱',
  `address` varchar(255) NOT NULL,
  `status` int(2) NOT NULL COMMENT '0-录入待审核 1-审核通过 2-审核不通过',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='商户申请表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `merchant_apply`
--

LOCK TABLES `merchant_apply` WRITE;
/*!40000 ALTER TABLE `merchant_apply` DISABLE KEYS */;
INSERT INTO `merchant_apply` VALUES (1,'2018-10-14 14:49:28','2018-10-14 14:49:28','甜辣香天下','甜辣香天下','刘超','500191199102133991','AJ78BN346LK08B6KBFP','130838389992','023-58246657','liuchaoheling@qq.com','重庆市万州区白山大道57号',1),(2,'2018-10-14 22:21:02','2018-10-14 22:21:02','刘一手火锅','刘一手','刘超','500191199102133991','AJ78BN346LK08B6KBFP','13038389992','023-58246657','liuchaoheling@qq.com','重庆市渝北区两江新区132号',0),(3,'2018-10-14 22:22:44','2018-10-14 22:22:44','幸福西饼视频有限公司','幸福西饼','何亮','500110100189198022','HASK88AS1JLK8lHGK4FJ5KJBC','13038322221','021-33888722','heliang@qq.com','上海市浦东新区玉兰路258弄',0);
/*!40000 ALTER TABLE `merchant_apply` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-15 17:29:50
