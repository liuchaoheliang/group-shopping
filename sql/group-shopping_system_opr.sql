-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: group-shopping
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `system_opr`
--

DROP TABLE IF EXISTS `system_opr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_opr` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '数据主键',
  `create_time` datetime NOT NULL COMMENT '数据创建时间',
  `update_time` datetime NOT NULL COMMENT '数据更新时间',
  `merchant_id` bigint(20) DEFAULT NULL COMMENT '所属商户',
  `nick_name` char(20) NOT NULL COMMENT '昵称',
  `user_name` char(20) NOT NULL COMMENT '用户账号名',
  `password` varchar(32) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1' COMMENT '用户状态 0-可用;1-不可用',
  `phone` varchar(20) DEFAULT NULL COMMENT '联系手机',
  `email` varchar(30) DEFAULT NULL COMMENT '联系邮箱',
  `type` int(11) NOT NULL COMMENT '1-后台管理员 2-商户操作员',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nickname` (`nick_name`),
  UNIQUE KEY `username` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=100000004 DEFAULT CHARSET=utf8 COMMENT='管理平台用户';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_opr`
--

LOCK TABLES `system_opr` WRITE;
/*!40000 ALTER TABLE `system_opr` DISABLE KEYS */;
INSERT INTO `system_opr` VALUES (100000000,'2018-09-10 18:45:02','2018-09-10 18:45:02',2,'痴心绝对','liuchao','MTExMTEx',0,'13039076991','abcs@qq.com',2),(100000001,'2018-09-10 18:45:02','2018-09-10 18:45:03',1,'admin','admin','MTExMTEx',0,'13010308888','admin@qq.com',1),(100000002,'2018-10-08 18:03:05','2018-10-08 18:03:05',3,'heliang','heliang','MTExMTEx',0,'13789129991','heliang@qq.com',2),(100000003,'2018-10-08 20:16:52','2018-10-08 20:16:52',4,'陈倩','chenqian','MTExMTEx',0,'13039391112','chenan@qq.com',2);
/*!40000 ALTER TABLE `system_opr` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-15 17:29:51
