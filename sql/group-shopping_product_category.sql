-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: group-shopping
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product_category`
--

DROP TABLE IF EXISTS `product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '数据主键',
  `create_time` datetime NOT NULL COMMENT '数据创建时间',
  `update_time` datetime NOT NULL COMMENT '数据更新时间',
  `category_name` varchar(10) NOT NULL COMMENT '分类名称',
  `status` char(1) DEFAULT NULL COMMENT '数据状态(1:可用 2:不可用 )',
  `parent_category_id` bigint(20) NOT NULL DEFAULT '-1' COMMENT '父级ID',
  `tree_path` varchar(255) DEFAULT NULL COMMENT '当前分类path tree',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000016 DEFAULT CHARSET=utf8 COMMENT='基础商品分类表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_category`
--

LOCK TABLES `product_category` WRITE;
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
INSERT INTO `product_category` VALUES (10000007,'2018-10-03 21:31:24','2018-10-03 21:31:24','男士精品','1',-1,NULL),(10000008,'2018-10-03 21:31:37','2018-10-03 21:31:37','女士优选','1',-1,NULL),(10000009,'2018-10-03 21:32:22','2018-10-03 21:32:22','儿童套装','1',-1,NULL),(10000010,'2018-10-03 21:32:33','2018-10-03 21:32:33','顶级时尚','1',-1,NULL),(10000011,'2018-10-03 21:33:09','2018-10-03 21:33:09','潮流T恤','1',-1,NULL),(10000012,'2018-10-03 21:33:26','2018-10-03 21:33:26','夏季系列','1',-1,NULL),(10000013,'2018-10-03 21:34:19','2018-10-03 21:34:19','时尚2018','1',-1,NULL),(10000014,'2018-10-03 21:34:56','2018-10-03 21:34:56','特价销售','1',-1,NULL),(10000015,'2018-10-03 21:43:29','2018-10-03 21:43:29','电子数码','1',-1,NULL);
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-15 17:29:50
