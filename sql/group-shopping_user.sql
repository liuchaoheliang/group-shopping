-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: group-shopping
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '数据主键',
  `create_time` datetime NOT NULL COMMENT '数据创建时间',
  `update_time` datetime NOT NULL COMMENT '数据更新时间',
  `nick_name` varchar(45) NOT NULL COMMENT '昵称',
  `login_id` varchar(45) NOT NULL COMMENT '登录名',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `status` int(11) NOT NULL COMMENT '0 -可用，1-不可用',
  `phone` varchar(45) NOT NULL COMMENT '电话',
  `email` varchar(45) DEFAULT NULL COMMENT '邮箱地址',
  `point` int(11) NOT NULL DEFAULT '0' COMMENT '积分',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='用户信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'2018-10-08 20:46:17','2018-10-08 20:46:17','测试用户','user','111111',0,'13038389992','user@qq.com',0),(2,'2018-10-10 11:04:21','2018-10-10 11:04:21','何亮','heliang','111111',0,'13939398888',NULL,0),(3,'2018-10-10 11:14:10','2018-10-10 11:14:10','痴心绝对','liuchao','222222',0,'13939398888','user@qq.com',0),(4,'2018-10-15 16:17:27','2018-10-15 16:17:27','user1','user1','111111',0,'13243439991','user1@qq.com',0),(5,'2018-10-15 16:17:27','2018-10-15 16:17:27','wangwu','wangwu','111111',0,'13288446622','user1@qq.com',0),(6,'2018-10-15 16:17:27','2018-10-15 16:17:27','zhangsan','zhangsan','111111',0,'13243439991','user1@qq.com',0),(7,'2018-10-15 16:17:27','2018-10-15 16:17:27','lisi','lisi','111111',0,'15723288493','user1@qq.com',0),(8,'2018-10-15 16:17:27','2018-10-15 16:17:27','lijing','lijing','111111',0,'17823233455','user1@qq.com',0),(9,'2018-10-15 16:17:27','2018-10-15 16:17:27','tuhao','tuhao','111111',0,'15823235533','user1@qq.com',0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-15 17:29:51
