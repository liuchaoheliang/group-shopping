/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.dao.entity.UserAddress;
import com.cloud.lc.group.shopping.dao.entity.UserAddressExample;
import com.cloud.lc.group.shopping.dao.mapper.UserAddressMapper;
import com.cloud.lc.group.shopping.service.UserAddressService;

@Service
public class UserAddressServiceImpl implements UserAddressService {

    @Resource
    private UserAddressMapper userAddressMapper;

    @Override
    public long insert(UserAddress userAddress) {
        userAddressMapper.insertSelective(userAddress);
        return userAddress.getId();
    }

    @Override
    public UserAddress getById(Long id) {
        return userAddressMapper.selectByPrimaryKey(id);
    }

    @Override
    public void deleteById(Long id) {
        userAddressMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void updateById(UserAddress userAddress) {
        userAddressMapper.updateByPrimaryKeySelective(userAddress);
    }

    @Override
    public List<UserAddress> getByConditions(UserAddressExample userAddress) {
        return userAddressMapper.selectByExample(userAddress);
    }

    @Override
    public Page<UserAddress> getByPage(Page<UserAddress> page) {
        return null;
    }
}
