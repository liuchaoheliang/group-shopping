/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.dao.entity;

import java.io.Serializable;

import org.apache.ibatis.type.Alias;

import com.cloud.lc.group.shopping.base.entity.BaseEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */

@Alias("systemOpr")
@ApiModel(value="系统操作员",description="系统操作员用户")
public class SystemOpr extends BaseEntity implements Serializable {

    @ApiModelProperty(value = "昵称")
    private  String nickName;

    @ApiModelProperty(value = "用户名")
    private  String userName;

    @ApiModelProperty(value = "密码")
    private transient String password;

    @ApiModelProperty(value = "状态")
    private Boolean status = false;

    @ApiModelProperty(value = "类型：1 系统管理员，2商户操作员")
    private Integer type;

    @ApiModelProperty(value = "手机号")
    private  String phone;

    @ApiModelProperty(value = "商户ID")
    private Long merchantId;

    private Merchant merchant;

    @ApiModelProperty(value = "邮箱")
    private  String email;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public SystemOpr withPassword(String password) {
        this.password = password;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public SystemOpr withUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public Boolean getStatusStatus() {
        return status;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public Merchant getMerchant() {
        return merchant;

    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }
}