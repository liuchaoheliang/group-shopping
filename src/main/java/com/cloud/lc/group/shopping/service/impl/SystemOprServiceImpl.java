/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.dao.entity.SystemOpr;
import com.cloud.lc.group.shopping.dao.mapper.SystemOprMapper;
import com.cloud.lc.group.shopping.service.SystemOprService;
import com.cloud.lc.group.shopping.util.SecureUtil;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/11
 * @since 1.0.0
 */
@Service
public class SystemOprServiceImpl implements SystemOprService {

    @Resource
    private SystemOprMapper systemOprMapper;

    @Override
    public long insert(SystemOpr systemOpr) {
        systemOpr.setPassword(SecureUtil.encodeBase64(systemOpr.getPassword()));
        systemOprMapper.insert(systemOpr);
        return systemOpr.getId();
    }

    @Override
    public SystemOpr getById(Long id) {
        return systemOprMapper.getById(id);
    }

    @Override
    public void deleteById(Long id) {
        systemOprMapper.deleteById(id);
    }

    @Override
    public void updateById(SystemOpr systemOpr) {
        if(systemOpr.getPassword() != null){
            systemOpr.setPassword(SecureUtil.encodeBase64(systemOpr.getPassword()));
        }
        systemOprMapper.updateById(systemOpr);
    }

    @Override
    public List<SystemOpr> getByConditions(SystemOpr systemOpr) {
        if (systemOpr.getPassword() != null) {
            systemOpr.setPassword(SecureUtil.encodeBase64(systemOpr.getPassword()));
        }
        return systemOprMapper.getByConditions(systemOpr);
    }

    @Override
    public Page<SystemOpr> getByPage(Page<SystemOpr> page) {
        List<SystemOpr> list = systemOprMapper.getByPage(page);
        page.setResultsContent(list);
        return page;
    }

}