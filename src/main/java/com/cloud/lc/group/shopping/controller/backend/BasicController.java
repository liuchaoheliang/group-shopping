/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.backend;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.cloud.lc.group.shopping.constant.WebConstant;
import com.cloud.lc.group.shopping.dao.entity.SystemOpr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Component;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloud.lc.group.shopping.dto.Result;
import com.cloud.lc.group.shopping.util.CookieWrap;

/**
 * 基础Controller提供一些基础的框架函数或者业务方法
 */
@Component
public class BasicController {

    protected static final Logger logger = LoggerFactory.getLogger(BasicController.class);

    @Autowired
    private HttpServletRequest request;

    /**
     * 判断是否为管理员
     * 
     * @return
     */
    public boolean isAdmin() {
        boolean flag = false;
        SystemOpr systemOpr = getLoginSysUser();
        if (systemOpr.getType() == 1) {
            flag = true;
        }
        return flag;
    }

    /**
     * 获取登录用户的所属商户Id
     * 
     * @return
     */
    public Long getMerchantId() {
        return getLoginSysUser().getMerchantId();
    }

    /**
     * 获取登录用户信息
     *
     * @return
     */
    public SystemOpr getLoginSysUser() {
        Map<String, String> userInfo =
                getCookieValue(request, WebConstant.COOKIE_NAME_MGMT_UID, WebConstant.COOKIE_NAME_MGMG_UINFO);
        SystemOpr systemOpr = null;
        try {
            systemOpr = JSON.parseObject(URLDecoder.decode(userInfo.get(WebConstant.COOKIE_NAME_MGMG_UINFO), "utf-8"),
                    SystemOpr.class);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return systemOpr;
    }

    /**
     * 基于@ExceptionHandler异常处理
     */
    @ExceptionHandler
    public @ResponseBody Result exceptionWrap(HttpServletRequest request, Exception e) {
        if (e instanceof HttpMediaTypeNotSupportedException) {
            // content-type 异常错误
            return Result.fail("数据类型不支持");
        } else if (e instanceof HttpMessageNotReadableException || e instanceof HttpMessageNotReadableException) {
            // post 请求数据错误
            return Result.fail("");
        }

        logger.error("controller exceptionWrap捕捉到异常", e);
        return Result.fail(e.getMessage());
    }

    /**
     * 获取单个或多个cookie值
     *
     * @param req
     * @param cookieNames
     * @return
     */
    protected HashMap<String, String> getCookieValue(HttpServletRequest req, String...cookieNames) {
        if (req == null || cookieNames == null || cookieNames.length == 0) {
            logger.error("获取cookies传入无效参数");
            return null;
        }
        Cookie[] cookies = req.getCookies();
        if (cookies == null || cookies.length == 0) {
            return null;
        }
        HashMap<String, String> cookieKV = new HashMap<>();
        for (Cookie cookie : cookies) {
            for (String cookieName : cookieNames) {
                if (cookie.getName().equals(cookieName)) {
                    cookieKV.put(cookieName, cookie.getValue());
                }
            }
        }
        return cookieKV;
    }

    /**
     * response 写回cookie
     *
     * @param res
     * @param cookieWrap
     */
    protected void setCookies(HttpServletResponse res, CookieWrap cookieWrap) {
        if (cookieWrap == null) {
            return;
        }
        for (Cookie cookie : cookieWrap.getCookies()) {
            res.addCookie(cookie);
        }
    }

    /**
     * 移除cookies
     *
     * @param res
     * @param cookiesName
     */
    protected void removeCookies(HttpServletRequest req, HttpServletResponse res, String...cookiesName) {
        if (cookiesName == null || cookiesName.length == 0) {
            return;
        }
        Cookie[] cookies = req.getCookies();
        Cookie deleteCookie = null;
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                for (String cookieName : cookiesName) {
                    if (cookieName.equals(cookie.getName())) {
                        deleteCookie = new Cookie(cookieName, null);
                        deleteCookie.setPath("/");
                        deleteCookie.setMaxAge(0);
                        res.addCookie(deleteCookie);
                    }
                }
            }
        }
    }
}
