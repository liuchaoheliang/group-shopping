/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.backend;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.cloud.lc.group.shopping.dao.entity.SystemOpr;
import com.cloud.lc.group.shopping.dto.form.MerchantForm;
import com.cloud.lc.group.shopping.dto.vo.MerchantApplyDto;
import com.cloud.lc.group.shopping.dto.vo.ProductDto;
import com.cloud.lc.group.shopping.util.XBeanUtil;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cloud.lc.group.shopping.annotation.NoLogin;
import com.cloud.lc.group.shopping.base.page.Order;
import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.base.page.PageFilter;
import com.cloud.lc.group.shopping.constant.WebConstant;
import com.cloud.lc.group.shopping.controller.support.ProductSupport;
import com.cloud.lc.group.shopping.dto.Result;
import com.cloud.lc.group.shopping.dto.form.QueryProductForm;
import com.cloud.lc.group.shopping.dto.form.SaveProductForm;
import com.cloud.lc.group.shopping.dto.vo.SystemOprDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */

@RestController
@Api(value = "商品相关接口", tags = { "商品相关接口" })
@RequestMapping(value = WebConstant.SYSTEM_BACKEND_PRIFIX + "/product")
public class ProductController extends BasicController {

    @Resource
    private ProductSupport productSupport;

    @PostMapping(value = "/save")
    @ApiOperation("保存商品")
    public Result save(HttpServletRequest request, @RequestBody @ApiParam SaveProductForm saveProductForm) {
        SystemOpr user = getLoginSysUser();
        saveProductForm.setMerchantId(user.getMerchantId());
        return Result.succ(productSupport.saveProduct(saveProductForm));
    }

    @GetMapping(value = "/page")
    @ApiOperation("系统用户分页查询")
    @NoLogin
    public Page<SystemOprDto> getByPage(Page page, @ApiParam QueryProductForm queryProductForm) {
        if (page == null) {
            page = new Page();
        }

        if (queryProductForm.getPage() != null) {
            page.setPageNumber(queryProductForm.getPage());
        }
        if (queryProductForm.getRows() != null) {
            page.setPageSize(queryProductForm.getRows());
        }

        if (page.getPageFilter() == null) {
            PageFilter pageFilterDto = new PageFilter();
            // pageFilterDto.setStartTime(DateUtils.addDays(new Date(), -30));
            // pageFilterDto.setEndTime(new Date());
            page.setPageFilter(pageFilterDto);
        }
        if (!isAdmin()) {
            queryProductForm.setMerchantId(getMerchantId());
        }
        page.getPageFilter().setFilterEntity(queryProductForm);

        // 排序
        Order order = new Order();
        order.setDirection(Order.Direction.desc);
        order.setProperty("create_time");
        page.setOrder(order);

        return productSupport.getProductByPage(page);
    }

    @GetMapping(value = "/detail")
    @ApiOperation("商品详情查询")
    public Result getByPage(Long productId) {
        return Result.succ(productSupport.productDetail(productId));
    }

    /**
     * @return
     */
    @GetMapping(value = "/aduit")
    @ApiOperation("审核商品")
    public Result aduit(Long productId, Integer status) {
        ProductDto productDto = productSupport.productDetail(productId);
        if (productDto.getStatus() != 1) {
            return Result.fail("当前申请已被处理过");
        }
        SaveProductForm saveProductForm = new SaveProductForm();
        saveProductForm.setId(productId);
        saveProductForm.setStatus(status);
        productSupport.saveProduct(saveProductForm);
        return Result.succ("处理成功");
    }

}
