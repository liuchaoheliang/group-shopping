/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.backend;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cloud.lc.group.shopping.constant.WebConstant;

import io.swagger.annotations.Api;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */

@RestController
@Api(value = "支付信息相关接口", tags = { "支付信息相关接口" })
@RequestMapping(value = WebConstant.SYSTEM_BACKEND_PRIFIX + "/product")
public class PayInfoController {

}