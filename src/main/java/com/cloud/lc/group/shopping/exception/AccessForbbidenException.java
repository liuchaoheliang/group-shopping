/*
 * Copyright (C) 2019 Baidu, Inc. All Rights Reserved.
 */

package com.cloud.lc.group.shopping.exception;

/**
 * 禁止访问
 *
 * @author zhangbi617
 * @date 2017-06-16
 */
public class AccessForbbidenException extends SystemException {

    private static final long serialVersionUID = 5984496421241575573L;

    public AccessForbbidenException(String message) {
        super(message);
    }
}
