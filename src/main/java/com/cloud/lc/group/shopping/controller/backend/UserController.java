/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.backend;

import com.cloud.lc.group.shopping.base.page.Order;
import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.base.page.PageFilter;
import com.cloud.lc.group.shopping.controller.support.UserSupport;
import com.cloud.lc.group.shopping.dto.Result;
import com.cloud.lc.group.shopping.dto.form.QueryOprUserForm;
import com.cloud.lc.group.shopping.dto.form.QueryUserForm;
import com.cloud.lc.group.shopping.dto.form.SaveOprUserForm;
import com.cloud.lc.group.shopping.dto.form.SaveUserForm;
import com.cloud.lc.group.shopping.dto.vo.SystemOprDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cloud.lc.group.shopping.constant.WebConstant;

import io.swagger.annotations.Api;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */

@RestController
@Api(value = "用户相关接口", tags = { "用户相关接口" })
@RequestMapping(value = WebConstant.SYSTEM_BACKEND_PRIFIX + "/user")
public class UserController {

    @Autowired
    private UserSupport userSupport;

    @GetMapping("/page")
    @ApiOperation("用户分页查询")
    public Page<SystemOprDto> getByPage(Page page, @ApiParam QueryUserForm queryUserForm) {

        if (page == null) {
            page = new Page();
        }
        if (page.getPageFilter() == null) {
            PageFilter pageFilterDto = new PageFilter();
            pageFilterDto.setStartTime(DateUtils.addDays(new Date(), -30));
            pageFilterDto.setEndTime(new Date());
            page.setPageFilter(pageFilterDto);
        }
        page.getPageFilter().setFilterEntity(queryUserForm);

        // 排序
        Order order = new Order();
        order.setDirection(Order.Direction.desc);
        order.setProperty("create_time");
        page.setOrder(order);

        return userSupport.getByPage(page);
    }

    @PostMapping("/save")
    @ApiOperation("添加用户")
    public Result save(@RequestBody @ApiParam SaveUserForm saveUserForm)
            throws InvocationTargetException, IllegalAccessException {
        userSupport.save(saveUserForm);
        return Result.succ(true);
    }

    @PostMapping("/update")
    @ApiOperation("修改用户")
    public Result update(@RequestBody @ApiParam SaveUserForm saveUserForm)
            throws InvocationTargetException, IllegalAccessException {
        userSupport.save(saveUserForm);
        return Result.succ(true);
    }
}