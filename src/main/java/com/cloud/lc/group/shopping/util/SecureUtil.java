/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.util;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

import com.cloud.lc.group.shopping.logger.Logger;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/11
 * @since 1.0.0
 */

public class SecureUtil {

    public static String encodeBase64(String str){
        return Base64.getEncoder().encodeToString(str.getBytes());
    }

    public static String decodeBase64(String str){
        try {
            return new String(Base64.getDecoder().decode(str.getBytes()),"utf-8");
        } catch (UnsupportedEncodingException e) {
            Logger.error(e.getMessage());
        }
        return "";
    }

}