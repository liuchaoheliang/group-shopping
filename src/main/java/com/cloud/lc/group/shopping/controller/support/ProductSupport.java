/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.support;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.cloud.lc.group.shopping.dto.vo.MerchantBreifDto;
import com.cloud.lc.group.shopping.dto.vo.MerchantDto;
import org.springframework.stereotype.Component;

import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.dao.entity.Product;
import com.cloud.lc.group.shopping.dao.entity.ProductCategory;
import com.cloud.lc.group.shopping.dao.entity.ProductCategoryExample;
import com.cloud.lc.group.shopping.dto.form.SaveProductCategoryForm;
import com.cloud.lc.group.shopping.dto.form.SaveProductForm;
import com.cloud.lc.group.shopping.dto.vo.ProductDto;
import com.cloud.lc.group.shopping.logger.Logger;
import com.cloud.lc.group.shopping.service.ProductCategoryService;
import com.cloud.lc.group.shopping.service.ProductService;
import com.cloud.lc.group.shopping.util.XBeanUtil;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/11
 * @since 1.0.0
 */

@Component
public class ProductSupport {

    @Resource
    private ProductCategoryService productCategoryService;

    @Resource
    private ProductService productService;

    private ProductDto domianToDto(Product product) {
        ProductDto productDto = new ProductDto();
        try {
            XBeanUtil.copyPropertiesIgnoresNull(productDto, product);

            MerchantBreifDto merchantDto = new MerchantBreifDto();
            if (product.getMerchant() != null) {
                XBeanUtil.copyPropertiesIgnoresNull(merchantDto, product.getMerchant());
            }
            productDto.setMerchant(merchantDto);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return productDto;
    }

    public Page getProductByPage(Page reqPage) {
        Page<Product> resPage = productService.getByPage(reqPage);
        List<ProductDto> resContents = resPage.getResultsContent().stream().map(systemOpr -> domianToDto(systemOpr))
                .collect(Collectors.toList());
        reqPage.setTotalCount(resPage.getTotalCount());
        reqPage.setPageCount(resPage.getPageCount());
        reqPage.setResultsContent(resContents);
        return reqPage;
    }

    public boolean saveProductCategory(SaveProductCategoryForm productCategoryForm) {
        ProductCategory productCategory = new ProductCategory();
        if (productCategoryForm != null && productCategoryForm.getId() == null) {
            productCategory.setCategoryName(productCategoryForm.getCategoryName());
            productCategory.setParentCategoryId(
                    productCategoryForm.getParentCategoryId() != null ? productCategoryForm.getParentCategoryId()
                            : -1L);
            productCategory.setStatus("1");
            productCategoryService.insert(productCategory);
        } else if (productCategoryForm != null && productCategoryForm.getId() != null) {
            productCategory.setId(productCategoryForm.getId());
            productCategory.setCategoryName(productCategoryForm.getCategoryName());
            productCategoryService.updateById(productCategory);
        }
        return true;
    }

    public boolean saveProduct(SaveProductForm saveProductForm) {
        Product product = new Product();
        try {
            XBeanUtil.copyPropertiesIgnoresEmpty(product, saveProductForm);
            if (product.getMarketPrice() != null) {
                product.setMarketPrice(new BigDecimal(saveProductForm.getMarketPrice() * 100).intValue());
            }
            if (product.getPrice() != null) {
                product.setPrice(new BigDecimal(saveProductForm.getPrice() * 100).intValue());
            }
            if (saveProductForm != null && saveProductForm.getId() == null) {
                productService.insert(product);
            } else if (saveProductForm != null && saveProductForm.getId() != null) {
                productService.updateById(product);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            Logger.error("保存商品出错", e);
            return false;
        }
        return true;
    }

    public boolean deleteProductCategory(Long id) {
        ProductCategory productCategory = new ProductCategory();
        productCategory.setId(id);
        productCategory.setStatus("2");
        productCategoryService.updateById(productCategory);
        return true;

    }

    public List<ProductCategory> getCategoryByParentId(Long id) {
        ProductCategoryExample productCategoryExample = new ProductCategoryExample();
        // 默认查询状态为1
        productCategoryExample.createCriteria().andStatusEqualTo("1").andParentCategoryIdEqualTo(id);
        return productCategoryService.getByConditions(productCategoryExample);
    }

    public ProductDto productDetail(Long id) {
        return domianToDto(productService.getById(id));
    }


}