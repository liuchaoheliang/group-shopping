/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.backend;

import com.cloud.lc.group.shopping.annotation.NoLogin;
import com.cloud.lc.group.shopping.base.page.Order;
import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.base.page.PageFilter;
import com.cloud.lc.group.shopping.controller.support.OrderSupport;
import com.cloud.lc.group.shopping.dto.form.QueryOrderForm;
import com.cloud.lc.group.shopping.dto.form.QueryProductForm;
import com.cloud.lc.group.shopping.dto.vo.OrderDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cloud.lc.group.shopping.constant.WebConstant;

import io.swagger.annotations.Api;

import javax.annotation.Resource;
import java.util.Date;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */

@RestController
@Api(value = "订单相关接口", tags = { "订单相关接口" })
@RequestMapping(value = WebConstant.SYSTEM_BACKEND_PRIFIX + "/order")
public class OrderController extends BasicController {

    @Resource
    private OrderSupport orderSupport;

    @GetMapping(value = "/page")
    @ApiOperation("订单分页查询")
    @NoLogin
    public Page<OrderDto> getByPage(Page page, @ApiParam QueryOrderForm queryOrderForm) {
        if (page == null) {
            page = new Page();
        }

        if (queryOrderForm.getPage() != null) {
            page.setPageNumber(queryOrderForm.getPage());
        }
        if (queryOrderForm.getRows() != null) {
            page.setPageSize(queryOrderForm.getRows());
        }

        if (page.getPageFilter() == null) {
            PageFilter pageFilterDto = new PageFilter();
            // pageFilterDto.setStartTime(DateUtils.addDays(new Date(), -30));
            // pageFilterDto.setEndTime(new Date());
            page.setPageFilter(pageFilterDto);
        }

        if (!isAdmin()) {
            queryOrderForm.setMerchantId(getMerchantId());
        }
        page.getPageFilter().setFilterEntity(queryOrderForm);

        // 排序
        Order order = new Order();
        order.setDirection(Order.Direction.desc);
        order.setProperty("create_time");
        page.setOrder(order);

        return orderSupport.getOderByPage(page);
    }

}