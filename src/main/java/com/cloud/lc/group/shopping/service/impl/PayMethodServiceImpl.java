/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.cloud.lc.group.shopping.dao.entity.User;
import org.springframework.stereotype.Service;

import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.dao.entity.PayMethod;
import com.cloud.lc.group.shopping.dao.entity.PayMethodExample;
import com.cloud.lc.group.shopping.dao.mapper.PayMethodMapper;
import com.cloud.lc.group.shopping.service.PayMethodService;

@Service
public class PayMethodServiceImpl implements PayMethodService {

    @Resource
    private PayMethodMapper payMethodMapper;

    @Override
    public long insert(PayMethod payMethod) {
        payMethodMapper.insertSelective(payMethod);
        return payMethod.getId();
    }

    @Override
    public PayMethod getById(Long id) {
        return payMethodMapper.selectByPrimaryKey(id);
    }

    @Override
    public void deleteById(Long id) {
        payMethodMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void updateById(PayMethod payMethod) {
        payMethodMapper.updateByPrimaryKeySelective(payMethod);
    }

    @Override
    public List<PayMethod> getByConditions(PayMethodExample payMethod) {
        return payMethodMapper.selectByExample(payMethod);
    }

    @Override
    public Page<PayMethod> getByPage(Page<PayMethod> page) {
        List<PayMethod> list = payMethodMapper.getByPage(page);
        page.setResultsContent(list);
        return page;
    }
}
