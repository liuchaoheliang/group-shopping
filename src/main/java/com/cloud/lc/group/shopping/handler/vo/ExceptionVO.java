/*
 * Copyright (C) 2019 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.handler.vo;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 异常VO
 *
 * @author LUYI374
 * @date 2017年2月17日
 * @since 1.0.0
 */
@ApiModel(description = "异常数据")
public class ExceptionVO {

    @ApiModelProperty(value = "异常Code,可选", example = "null")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String errorCode;

    @ApiModelProperty(value = "异常消息,可选", example = "参数不符合要求")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
