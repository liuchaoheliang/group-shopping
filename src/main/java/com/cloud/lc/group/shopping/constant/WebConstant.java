/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.constant;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */

public class WebConstant {

    public final static String GLOBAL_DEFAULT_CHARSET = "utf-8";

    // cookie 过期时间
    public final static int COOKIE_EXPIRED_SECOUND = 60 * 60;

    public final static String SYSTEM_FILE_SEPARATOR = "/";

    public final static String COOKIE_BASE_PATH = "/";

    // 系统接口前缀
    public static final String SYSTEM_BACKEND_PRIFIX = "/backend/api";
    public static final String SYSTEM_FRONTEND_PRIFIX = "/frontend/api";

    // 系统保存session key
    public static final String ADMIN_LOGIN_SESSION = "admin_login_session";
    public static final String USER_LOGIN_SESSION = "user_login_session";
    public static final String MERCHANT_LOGIN_SESSION = "merchant_login_session";

    // 管理平台cookie
    // cookie name of uid
    public final static String COOKIE_NAME_MGMT_UID = "mgmt_opr_id";
    // cookie name of token
    public final static String COOKIE_NAME_MGMT_TOKEN = "mgmt_opr_token";
    // cookie name of userinfo
    public final static String COOKIE_NAME_MGMG_UINFO = "mgmt_opr_info";

    // 商城cookie
    // cookie name of uid
    public final static String COOKIE_NAME_SHOP_UID = "shop_user_id";
    // cookie name of token
    public final static String COOKIE_NAME_SHOP_TOKEN = "shop_user_token";
    // cookie name of userinfo
    public final static String COOKIE_NAME_SHOP_UINFO = "shop_user_info";

    // resp code
    public static final int RESP_CODE_SUCCESS = 200;
    public static final int RESP_CODE_FAIL = 500;


    // msg code
    public static final int PASSWORD_ERROR = 999;
    public static final int NOT_LOGIN = 998;

}