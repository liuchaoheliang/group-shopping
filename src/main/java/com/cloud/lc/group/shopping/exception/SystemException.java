//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.cloud.lc.group.shopping.exception;

import com.cloud.lc.group.shopping.exception.BaseException;

public class SystemException extends BaseException {
    private static final long serialVersionUID = 427394729472912343L;

    public SystemException() {
    }

    public SystemException(String message, Throwable cause) {
        super(message, cause);
    }

    public SystemException(String message) {
        super(message);
    }

    public SystemException(String message, Object[] args, Throwable cause) {
        super(message, args, cause);
    }

    public SystemException(String message, Object[] args) {
        super(message, args);
    }

    public SystemException(Object errorCode, String message, Throwable cause) {
        super(errorCode, message, cause);
    }

    public SystemException(Object errorCode, String message) {
        super(errorCode, message);
    }

    public SystemException(Object errorCode, String message, Object[] args, Throwable cause) {
        super(errorCode, message, args, cause);
    }

    public SystemException(Object errorCode, String message, Object[] args) {
        super(errorCode, message, args);
    }

    public SystemException(Throwable cause) {
        super(cause);
    }
}
