/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.filter;

import com.cloud.lc.group.shopping.logger.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

public class CharsetFilter implements Filter {

	public void destroy() {
		Logger.debug("======销毁过滤器=========");
	}

	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
		long start = System.currentTimeMillis();
		Logger.debug("filter start ..." );
		arg0.setCharacterEncoding("utf-8");
		arg1.setCharacterEncoding("utf-8");
		arg2.doFilter(arg0, arg1);
		Logger.debug("filter end , 耗时：" + (System.currentTimeMillis() - start));
	}

	public void init(FilterConfig arg0) throws ServletException {
		Logger.debug("======初始化过滤器=========");
	}

}
