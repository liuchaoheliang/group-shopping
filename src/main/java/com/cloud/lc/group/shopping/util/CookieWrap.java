/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;

public class CookieWrap {

    private List<Cookie> cookies = new ArrayList<>();

    /**
     * 基础cookies批量设置
     * 作用域为 /
     *
     * @param cookiesKV
     */
    public CookieWrap(Map<String, String> cookiesKV, String path, int expireTimeSecound) {
        if (cookiesKV != null && cookiesKV.size() > 0) {
            Cookie cookie;
            for (Map.Entry<String, String> cookieKV : cookiesKV.entrySet()) {
                cookie = new Cookie(cookieKV.getKey(), cookieKV.getValue());
                cookie.setPath(path);
                cookie.setMaxAge(expireTimeSecound);
                cookies.add(cookie);
            }
        }
    }

    public CookieWrap(String key, String value) {
        if (key != null && key.trim().length() != 0) {
            cookies.add(new Cookie(key, value));
        }
    }

    public List<Cookie> getCookies() {
        return cookies;
    }
}
