/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.cloud.lc.group.shopping.dao.entity.SystemOpr;
import org.springframework.stereotype.Service;

import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.dao.entity.User;
import com.cloud.lc.group.shopping.dao.entity.UserExample;
import com.cloud.lc.group.shopping.dao.mapper.UserMapper;
import com.cloud.lc.group.shopping.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Resource
	private UserMapper userMapper;

	@Override
	public long insert(User user) {
        userMapper.insertSelective(user);
		return user.getId();
	}

	@Override
	public User getById(Long id) {
        return userMapper.selectByPrimaryKey(id);
	}

	@Override
	public void deleteById(Long id) {
        userMapper.deleteByPrimaryKey(id);
	}

	@Override
	public void updateById(User user) {
        userMapper.updateByPrimaryKeySelective(user);
	}

	@Override
    public List<User> getByConditions(UserExample user) {
        return userMapper.selectByExample(user);
	}

	@Override
	public Page<User> getByPage(Page<User> page) {
        List<User> list = userMapper.getByPage(page);
        page.setResultsContent(list);
        return page;
	}
}
