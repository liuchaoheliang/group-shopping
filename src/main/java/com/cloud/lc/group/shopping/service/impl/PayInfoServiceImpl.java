/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.dao.entity.PayInfo;
import com.cloud.lc.group.shopping.dao.entity.PayInfoExample;
import com.cloud.lc.group.shopping.dao.mapper.PayInfoMapper;
import com.cloud.lc.group.shopping.service.PayInfoService;

@Service
public class PayInfoServiceImpl implements PayInfoService {

    @Resource
    private PayInfoMapper payInfoMapper;

    @Override
    public long insert(PayInfo payInfo) {
        payInfoMapper.insertSelective(payInfo);
        return payInfo.getId();
    }

    @Override
    public PayInfo getById(Long id) {
        return payInfoMapper.selectByPrimaryKey(id);
    }

    @Override
    public void deleteById(Long id) {
        payInfoMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void updateById(PayInfo payInfo) {
        payInfoMapper.updateByPrimaryKeySelective(payInfo);
    }

    @Override
    public List<PayInfo> getByConditions(PayInfoExample payInfo) {
        return payInfoMapper.selectByExample(payInfo);
    }

    @Override
    public Page<PayInfo> getByPage(Page<PayInfo> page) {
        return null;
    }
}
