/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.frontend;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.cloud.lc.group.shopping.constant.WebConstant;
import com.cloud.lc.group.shopping.dto.Result;
import com.cloud.lc.group.shopping.logger.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */

@RestController(value = "CommonController")
@RequestMapping(value = WebConstant.SYSTEM_FRONTEND_PRIFIX + "/common")
public class CommonController {

    @Value("${upload.images.path}")
    private String uploadImagesPath;

    @Value("${image.address}")
    private String imageAddress;

    @GetMapping(value = "fetchImage")
    public void fetchImage(HttpServletRequest request, HttpServletResponse res, String ImageId) {
        // 判断文件是否为空
        String fileName = "";
        String imagesPath = "";
        String filePath = "";

    }
}