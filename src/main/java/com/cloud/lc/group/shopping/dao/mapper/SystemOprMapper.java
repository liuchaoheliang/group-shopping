/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.dao.mapper;

import com.cloud.lc.group.shopping.base.mapper.BaseMapper;
import com.cloud.lc.group.shopping.dao.entity.SystemOpr;
import com.cloud.lc.group.shopping.dao.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SystemOprMapper extends BaseMapper<SystemOpr> {

}
