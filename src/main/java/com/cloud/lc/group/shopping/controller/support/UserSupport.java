/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.support;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.cloud.lc.group.shopping.dao.entity.User;
import com.cloud.lc.group.shopping.dao.entity.UserAddress;
import com.cloud.lc.group.shopping.dao.entity.UserAddressExample;
import com.cloud.lc.group.shopping.dao.entity.UserExample;
import com.cloud.lc.group.shopping.dto.Result;
import com.cloud.lc.group.shopping.dto.form.RegisterUserForm;
import com.cloud.lc.group.shopping.dto.form.SaveAddressForm;
import com.cloud.lc.group.shopping.dto.form.SaveOprUserForm;
import com.cloud.lc.group.shopping.dto.form.SaveUserForm;
import com.cloud.lc.group.shopping.dto.vo.UserDto;
import com.cloud.lc.group.shopping.service.UserAddressService;
import com.cloud.lc.group.shopping.service.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.dao.entity.SystemOpr;
import com.cloud.lc.group.shopping.dto.vo.SystemOprDto;
import com.cloud.lc.group.shopping.util.XBeanUtil;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/11
 * @since 1.0.0
 */

@Component
public class UserSupport {

    @Resource
    private UserService userService;

    @Resource
    private UserAddressService userAddressService;

    public Result registerUser(RegisterUserForm registerUserForm)
            throws InvocationTargetException, IllegalAccessException {
        User user = new User();
        // check user exist
        if (getByLoginId(registerUserForm.getLoginId()) != null) {
            return Result.fail("登录名已存在");
        }
        XBeanUtil.copyPropertiesIgnoresNull(user, registerUserForm);
        userService.insert(user);
        return Result.succ("注册成功");
    }

    /**
     * 查询用户是否存在
     *
     * @param loginId
     * @param password
     * @return
     */
    public UserDto checkUserExist(String loginId, String password) {
        UserDto userDto = new UserDto();
        UserExample userExample = new UserExample();

        if (loginId != null && password != null) {
            userExample.createCriteria().andLoginIdEqualTo(loginId).andPasswordEqualTo(password);
        } else if (loginId != null && password == null) {
            userExample.createCriteria().andLoginIdEqualTo(loginId);
        }
        List<com.cloud.lc.group.shopping.dao.entity.User> oprList = userService.getByConditions(userExample);
        if (CollectionUtils.isEmpty(oprList)) {
            return null;
        }
        try {
            XBeanUtil.copyPropertiesIgnoresNull(userDto, oprList.get(0));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return userDto;

    }

    /**
     * 查询用户信息
     *
     * @param id
     * @return
     */
    public UserDto getById(Long id) {
        UserDto userDto = new UserDto();
        User user = userService.getById(id);
        if (user == null) {
            return null;
        }
        try {
            XBeanUtil.copyPropertiesIgnoresNull(userDto, user);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return userDto;
    }

    /**
     * 查询用户信息
     *
     * @param loginId
     * @return
     */
    public UserDto getByLoginId(String loginId) {
        return checkUserExist(loginId, null);
    }

    public Page getByPage(Page reqPage) {
        Page<User> resPage = userService.getByPage(reqPage);
        List<UserDto> resContents =
                resPage.getResultsContent().stream().map(user -> domianToDto(user)).collect(Collectors.toList());
        reqPage.setTotalCount(resPage.getTotalCount());
        reqPage.setPageCount(resPage.getPageCount());
        reqPage.setResultsContent(resContents);
        return reqPage;
    }

    private UserDto domianToDto(User user) {
        UserDto userDto = new UserDto();
        try {
            XBeanUtil.copyPropertiesIgnoresNull(userDto, user);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return userDto;
    }

    public Result save(SaveUserForm saveUserForm) throws InvocationTargetException, IllegalAccessException {
        User user = new User();
        XBeanUtil.copyPropertiesIgnoresNull(user, saveUserForm);
        if (saveUserForm.getId() != null) {
            userService.updateById(user);
        } else {
            // check user exist
            if (getByLoginId(saveUserForm.getLoginId()) != null) {
                return Result.fail("用户名已存在");
            }
            userService.insert(user);
        }
        return Result.succ("保存成功");
    }

    public List<UserAddress> getAddresses(Long userId) {
        UserAddressExample addressExample = new UserAddressExample();
        addressExample.createCriteria().andUserIdEqualTo(userId);
        return userAddressService.getByConditions(addressExample);
    }

    public void saveAddresses(SaveAddressForm saveAddressForm)
            throws InvocationTargetException, IllegalAccessException {
        UserAddress userAddress = new UserAddress();
        XBeanUtil.copyPropertiesIgnoresNull(userAddress, saveAddressForm);
        if (userAddress.getId() != null) {
            userAddressService.updateById(userAddress);
        } else {
            userAddressService.insert(userAddress);
        }
    }

}