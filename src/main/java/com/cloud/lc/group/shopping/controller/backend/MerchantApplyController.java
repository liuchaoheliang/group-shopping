/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.backend;

import com.cloud.lc.group.shopping.annotation.NoLogin;
import com.cloud.lc.group.shopping.base.page.Order;
import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.base.page.PageFilter;
import com.cloud.lc.group.shopping.controller.support.MerchantSupport;
import com.cloud.lc.group.shopping.dto.Result;
import com.cloud.lc.group.shopping.dto.form.MerchantForm;
import com.cloud.lc.group.shopping.dto.form.QueryApplyForm;
import com.cloud.lc.group.shopping.dto.form.QueryProductForm;
import com.cloud.lc.group.shopping.dto.vo.MerchantApplyDto;
import com.cloud.lc.group.shopping.dto.vo.SystemOprDto;
import com.cloud.lc.group.shopping.util.XBeanUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cloud.lc.group.shopping.constant.WebConstant;

import io.swagger.annotations.Api;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */

@RestController
@Api(value = "商户申请相关接口", tags = { "商户申请相关接口" })
@RequestMapping(value = WebConstant.SYSTEM_BACKEND_PRIFIX + "/merchantApply")
public class MerchantApplyController {

    @Autowired
    private MerchantSupport merchantSupport;

    /**
     * @return
     */
    @GetMapping(value = "/aduit")
    @ApiOperation("审核商户申请")
    public Result aduitApply(Long applyId, Integer status) {
        MerchantApplyDto merchantApplyDto = merchantSupport.getApplyById(applyId);
        if (merchantApplyDto.getStatus() != 0) {
            return Result.fail("当前申请已被处理过");
        }
        merchantSupport.updateApplyById(applyId, status);
        if (status == 1) {
            MerchantForm merchantForm = new MerchantForm();
            try {
                XBeanUtil.copyPropertiesIgnoresNull(merchantForm, merchantApplyDto);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            merchantSupport.addMerchant(merchantForm);
        }
        return Result.succ("处理成功");
    }

    @GetMapping(value = "/page")
    @ApiOperation("商户接入申请分页查询")
    public Page<SystemOprDto> getByPage(Page page, @ApiParam QueryApplyForm queryApplyForm) {
        if (page == null) {
            page = new Page();
        }

        if (queryApplyForm.getPage() != null) {
            page.setPageNumber(queryApplyForm.getPage());
        }
        if (queryApplyForm.getRows() != null) {
            page.setPageSize(queryApplyForm.getRows());
        }

        if (page.getPageFilter() == null) {
            PageFilter pageFilterDto = new PageFilter();
            // pageFilterDto.setStartTime(DateUtils.addDays(new Date(), -30));
            // pageFilterDto.setEndTime(new Date());
            page.setPageFilter(pageFilterDto);
        }
        page.getPageFilter().setFilterEntity(queryApplyForm);

        // 排序
        Order order = new Order();
        order.setDirection(Order.Direction.desc);
        order.setProperty("create_time");
        page.setOrder(order);

        return merchantSupport.getApplyByPage(page);
    }
}