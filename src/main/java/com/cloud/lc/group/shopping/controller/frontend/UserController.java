/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.frontend;

import com.cloud.lc.group.shopping.annotation.NoLogin;
import com.cloud.lc.group.shopping.controller.support.UserSupport;
import com.cloud.lc.group.shopping.dto.Result;
import com.cloud.lc.group.shopping.dto.form.ModifyPwdForm;
import com.cloud.lc.group.shopping.dto.form.RegisterUserForm;
import com.cloud.lc.group.shopping.dto.form.SaveAddressForm;
import com.cloud.lc.group.shopping.dto.form.SaveUserForm;
import com.cloud.lc.group.shopping.dto.vo.UserDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cloud.lc.group.shopping.constant.WebConstant;

import io.swagger.annotations.Api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */

@RestController("UserController")
@Api(value = "用户相关接口", tags = { "用户相关接口" })
@RequestMapping(value = WebConstant.SYSTEM_FRONTEND_PRIFIX + "/user")
public class UserController extends BasicController {

    @Autowired
    private UserSupport userSupport;

    @PostMapping("register")
    @ApiOperation("用户注册")
    @NoLogin
    public Result register(@RequestBody @ApiParam RegisterUserForm registerUserForm)
            throws InvocationTargetException, IllegalAccessException {
        return userSupport.registerUser(registerUserForm);
    }

    @GetMapping("/userInfo")
    @ApiOperation("查询用户信息")
    public Result userInfo(HttpServletRequest request, HttpServletResponse res)
            throws InvocationTargetException, IllegalAccessException {
        return Result.succ(userSupport.getById(getSessionUserId(request)));
    }

    @GetMapping("/address")
    @ApiOperation("查询用户收货地址")
    public Result address(HttpServletRequest request, HttpServletResponse res)
            throws InvocationTargetException, IllegalAccessException {
        return Result.succ(userSupport.getAddresses(getSessionUserId(request)));
    }

    @PostMapping("/saveAddress")
    @ApiOperation("保存用户收货地址")
    public Result saveAddress(HttpServletRequest request, @RequestBody SaveAddressForm saveAddressForm)
            throws InvocationTargetException, IllegalAccessException {
        Long userId = getSessionUserId(request);
        saveAddressForm.setUserId(userId);
        userSupport.saveAddresses(saveAddressForm);
        return Result.succ("保存成功");
    }

    @PostMapping("/modifyPwd")
    @ApiOperation("修改用户密码")
    public Result modifyPwd(HttpServletRequest request, @RequestBody ModifyPwdForm modifyPwdForm)
            throws InvocationTargetException, IllegalAccessException {
        Long userId = getSessionUserId(request);
        UserDto userDto = userSupport.getById(userId);
        if (userDto.getPassword().equals(modifyPwdForm.getOldPwd())) {
            SaveUserForm userForm = new SaveUserForm();
            userForm.setId(userDto.getId());
            userForm.setPassword(modifyPwdForm.getPwd());
            userSupport.save(userForm);
            return Result.succ("修改密码成功");
        } else {
            return Result.fail("原密码输入不正确");
        }
    }

}