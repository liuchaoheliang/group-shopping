/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.frontend;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cloud.lc.group.shopping.constant.WebConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloud.lc.group.shopping.dto.Result;
import com.cloud.lc.group.shopping.util.CookieWrap;

/**
 * 基础Controller提供一些基础的框架函数或者业务方法
 */
public class BasicController {

    protected static final Logger logger = LoggerFactory.getLogger(BasicController.class);

    /**
     * 获取登录用户的ID
     * 
     * @param request
     * @return
     */
    public Long getSessionUserId(HttpServletRequest request) {

        Map<String, String> userInfo =
                getCookieValue(request, WebConstant.COOKIE_NAME_SHOP_UID, WebConstant.COOKIE_NAME_SHOP_UINFO);
        Long userId = Long.valueOf(userInfo.get(WebConstant.COOKIE_NAME_SHOP_UID));
        return userId;
    }

    /**
     * 基于@ExceptionHandler异常处理
     */
    @ExceptionHandler
    public @ResponseBody Result exceptionWrap(HttpServletRequest request, Exception e) {
        if (e instanceof HttpMediaTypeNotSupportedException) {
            // content-type 异常错误
            return Result.fail("数据类型不支持");
        } else if (e instanceof HttpMessageNotReadableException || e instanceof HttpMessageNotReadableException) {
            // post 请求数据错误
            return Result.fail("");
        }

        logger.error("controller exceptionWrap捕捉到异常", e);
        return Result.fail(e.getMessage());
    }

    /**
     * 获取单个或多个cookie值
     *
     * @param req
     * @param cookieNames
     * @return
     */
    protected HashMap<String, String> getCookieValue(HttpServletRequest req, String...cookieNames) {
        if (req == null || cookieNames == null || cookieNames.length == 0) {
            logger.error("获取cookies传入无效参数");
            return null;
        }
        Cookie[] cookies = req.getCookies();
        if (cookies == null || cookies.length == 0) {
            return null;
        }
        HashMap<String, String> cookieKV = new HashMap<>();
        for (Cookie cookie : cookies) {
            for (String cookieName : cookieNames) {
                if (cookie.getName().equals(cookieName)) {
                    cookieKV.put(cookieName, cookie.getValue());
                }
            }
        }
        return cookieKV;
    }

    /**
     * response 写回cookie
     *
     * @param res
     * @param cookieWrap
     */
    protected void setCookies(HttpServletResponse res, CookieWrap cookieWrap) {
        if (cookieWrap == null) {
            return;
        }
        for (Cookie cookie : cookieWrap.getCookies()) {
            res.addCookie(cookie);
        }
    }

    /**
     * 移除cookies
     *
     * @param res
     * @param cookiesName
     */
    protected void removeCookies(HttpServletRequest req, HttpServletResponse res, String...cookiesName) {
        if (cookiesName == null || cookiesName.length == 0) {
            return;
        }
        Cookie[] cookies = req.getCookies();
        Cookie deleteCookie = null;
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                for (String cookieName : cookiesName) {
                    if (cookieName.equals(cookie.getName())) {
                        deleteCookie = new Cookie(cookieName, null);
                        deleteCookie.setPath("/");
                        deleteCookie.setMaxAge(0);
                        res.addCookie(deleteCookie);
                    }
                }
            }
        }
    }
}
