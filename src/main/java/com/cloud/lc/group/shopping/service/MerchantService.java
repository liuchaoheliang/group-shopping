/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.service;

import java.util.List;

import com.cloud.lc.group.shopping.base.service.BaseService;
import com.cloud.lc.group.shopping.dao.entity.Merchant;
import com.cloud.lc.group.shopping.dao.entity.MerchantExample;

public interface MerchantService extends BaseService<Merchant, MerchantExample> {

}
