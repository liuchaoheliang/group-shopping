/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.support;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.cloud.lc.group.shopping.dto.form.SaveOprUserForm;
import com.cloud.lc.group.shopping.dto.vo.MerchantDto;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.dao.entity.SystemOpr;
import com.cloud.lc.group.shopping.dto.vo.SystemOprDto;
import com.cloud.lc.group.shopping.service.SystemOprService;
import com.cloud.lc.group.shopping.util.XBeanUtil;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/11
 * @since 1.0.0
 */

@Component
public class SystemOprSupport {

    @Resource
    private SystemOprService systemOprService;

    public void save(SaveOprUserForm saveOprUserForm) throws InvocationTargetException, IllegalAccessException {
        SystemOpr systemOpr = new SystemOpr();
        XBeanUtil.copyPropertiesIgnoresNull(systemOpr, saveOprUserForm);
        if (saveOprUserForm.getId() != null) {
            systemOprService.updateById(systemOpr);
        } else {
            systemOprService.insert(systemOpr);
        }
    }

    public SystemOpr checkOprExist(String loginId, String password){
        List<SystemOpr> oprList = systemOprService.getByConditions(new SystemOpr().withUserName(loginId).withPassword(password));
        if(CollectionUtils.isEmpty(oprList)){
            return null;
        }
        return oprList.get(0);
    }

    public Page getByPage(Page reqPage) {
        Page<SystemOpr> resPage = systemOprService.getByPage(reqPage);
        List<SystemOprDto> resContents = resPage.getResultsContent().stream().map(systemOpr -> domianToDto(systemOpr))
                .collect(Collectors.toList());
        reqPage.setTotalCount(resPage.getTotalCount());
        reqPage.setPageCount(resPage.getPageCount());
        reqPage.setResultsContent(resContents);
        return reqPage;
    }

    private SystemOprDto domianToDto(SystemOpr systemOpr) {
        SystemOprDto systemOprDto = new SystemOprDto();
        try {
            XBeanUtil.copyPropertiesIgnoresNull(systemOprDto, systemOpr);

            MerchantDto merchantDto = new MerchantDto();
            XBeanUtil.copyPropertiesIgnoresNull(merchantDto, systemOpr.getMerchant());
            systemOprDto.setMerchant(merchantDto);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return systemOprDto;
    }


}