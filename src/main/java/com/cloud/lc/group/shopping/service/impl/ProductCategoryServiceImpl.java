/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.dao.entity.ProductCategory;
import com.cloud.lc.group.shopping.dao.entity.ProductCategoryExample;
import com.cloud.lc.group.shopping.dao.mapper.ProductCategoryMapper;
import com.cloud.lc.group.shopping.service.ProductCategoryService;

@Service
public class ProductCategoryServiceImpl implements ProductCategoryService {

    @Resource
    private ProductCategoryMapper productCategoryMapper;

    @Override
    public long insert(ProductCategory productCategory) {
        productCategoryMapper.insertSelective(productCategory);
        return productCategory.getId();
    }

    @Override
    public ProductCategory getById(Long id) {
        return productCategoryMapper.selectByPrimaryKey(id);
    }

    @Override
    public void deleteById(Long id) {
        productCategoryMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void updateById(ProductCategory productCategory) {
        productCategoryMapper.updateByPrimaryKeySelective(productCategory);
    }

    @Override
    public List<ProductCategory> getByConditions(ProductCategoryExample productCategory) {
        return productCategoryMapper.selectByExample(productCategory);
    }

    @Override
    public Page<ProductCategory> getByPage(Page<ProductCategory> page) {
        return null;
    }
}
