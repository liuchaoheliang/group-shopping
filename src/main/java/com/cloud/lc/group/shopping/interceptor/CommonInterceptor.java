/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.interceptor;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.cloud.lc.group.shopping.annotation.NoLogin;
import com.cloud.lc.group.shopping.constant.WebConstant;
import com.cloud.lc.group.shopping.dto.Result;
import com.cloud.lc.group.shopping.logger.Logger;

/**
 * 自定义拦截器
 * @author LiuChao
 *
 */

@Component
public class CommonInterceptor implements HandlerInterceptor {

	/**
	 *最终 finish 之后
	 * @author LiuChao
	 *
	 */
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		Logger.info("========afterCompletion=========");
        Long start = (Long) arg0.getAttribute("startTime");
        Logger.info("耗时:"+(System.currentTimeMillis() - start));
        
	}

	/**
	 *  执行返回视图的时候
	 * @author LiuChao
	 *
	 */
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		Logger.info("========postHandle=========");
        Long start = (Long) arg0.getAttribute("startTime");
        if(arg3 != null){
        	Logger.info("model param: "+ JSON.toJSONString(arg3.getModel()) );
        	Logger.info("model  view: "+ arg3.getViewName() );
        }
        Logger.info("耗时:"+(System.currentTimeMillis() - start));
	}

	/**
	  * 执行之前
	 * @author LiuChao
	 *
	 */
	public boolean preHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2) throws Exception {
        HandlerMethod handlerMethod = (HandlerMethod) arg2;
        Class<?> clazz = handlerMethod.getBeanType();
        Method m = handlerMethod.getMethod();

		Logger.info("========preHandle=========");
        String requestURI = arg0.getRequestURI();
        if (requestURI.startsWith(WebConstant.SYSTEM_BACKEND_PRIFIX)) {
            if (!checkAdminLogin(arg0) && !isNoLogin(clazz, m)) {
                reponse(arg1, JSON.toJSONString(Result.fail(WebConstant.NOT_LOGIN, "请先登录")));
                return false;
            }
        } else if (requestURI.startsWith(WebConstant.SYSTEM_FRONTEND_PRIFIX)) {
            if (!checkUserLogin(arg0) && !isNoLogin(clazz, m)) {
                reponse(arg1, JSON.toJSONString(Result.fail(WebConstant.NOT_LOGIN, "请先登录")));
                return false;
            }
        }

        //Logger.info(((HandlerMethod)arg2).getBean().getClass().getName());
        //Logger.info(((HandlerMethod)arg2).getMethod().getName());
        
        arg0.setAttribute("startTime", System.currentTimeMillis());
        
        return true;
	}

    private boolean checkAdminLogin(HttpServletRequest arg0) {
        String uid = null;
        Cookie[] cookies = arg0.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (WebConstant.COOKIE_NAME_MGMT_UID.equals(cookie.getName())) {
                    uid = cookie.getValue();
                    if (uid != null && uid.length() > 0) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean checkUserLogin(HttpServletRequest arg0) {
        String uid = null;
        Cookie[] cookies = arg0.getCookies();
        if (cookies == null) {
            return false;
        }
        for (Cookie cookie : cookies) {
            if (WebConstant.COOKIE_NAME_SHOP_UID.equals(cookie.getName())) {
                uid = cookie.getValue();
                if (uid != null && uid.length() > 0) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 是否不需要登录
     *
     * @param clazz handler class
     * @param m handler method
     * @return
     */
    private boolean isNoLogin(Class<?> clazz, Method m) {
        return clazz.isAnnotationPresent(NoLogin.class) || m.isAnnotationPresent(NoLogin.class);
    }

    private void reponse(HttpServletResponse arg1, String msg) throws IOException {
        arg1.setStatus(200);
        arg1.getWriter().write(msg);
    }

}
