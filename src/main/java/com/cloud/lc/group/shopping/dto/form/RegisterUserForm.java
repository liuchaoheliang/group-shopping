/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.dto.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */

@ApiModel(description = "用户注册参数实体")
public class RegisterUserForm {

    @ApiModelProperty(value = "昵称")
    @NotNull
    private String nickName;

    @ApiModelProperty(value = "用户名")
    @NotNull
    private String loginId;

    @ApiModelProperty(value = "状态")
    private boolean status;

    @ApiModelProperty(value = "手机号")
    @NotNull
    private String phone;

    @ApiModelProperty(value = "邮箱")
    @NotNull
    private String email;

    @ApiModelProperty(value = "密码")
    @NotNull
    private String password;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}