/*
 * Copyright (C) 2019 Baidu, Inc. All Rights Reserved.
 */

package com.cloud.lc.group.shopping.exception;

/**
 * 未登录
 *
 * @author zhangbi617
 * @date 2017-06-16
 */
public class UnauthorizedException extends SystemException {

    private static final long serialVersionUID = 3658630407647407869L;

    public UnauthorizedException(String message) {
        super(message);
    }
}
