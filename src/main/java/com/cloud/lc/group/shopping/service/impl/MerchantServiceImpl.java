/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.cloud.lc.group.shopping.dao.entity.MerchantApply;
import org.springframework.stereotype.Service;

import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.dao.entity.Merchant;
import com.cloud.lc.group.shopping.dao.entity.MerchantExample;
import com.cloud.lc.group.shopping.dao.mapper.MerchantMapper;
import com.cloud.lc.group.shopping.service.MerchantService;

@Service
public class MerchantServiceImpl implements MerchantService {

    @Resource
    private MerchantMapper merchantMapper;

    @Override
    public long insert(Merchant merchant) {
        merchantMapper.insertSelective(merchant);
        return merchant.getId();
    }

    @Override
    public Merchant getById(Long id) {
        return merchantMapper.selectByPrimaryKey(id);
    }

    @Override
    public void deleteById(Long id) {
        merchantMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void updateById(Merchant merchant) {
        merchantMapper.updateByPrimaryKeySelective(merchant);
    }

    @Override
    public List<Merchant> getByConditions(MerchantExample merchant) {
        return merchantMapper.selectByExample(merchant);
    }

    @Override
    public Page<Merchant> getByPage(Page<Merchant> page) {
        List<Merchant> list = merchantMapper.getByPage(page);
        page.setResultsContent(list);
        return page;
    }
}
