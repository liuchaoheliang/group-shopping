/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.frontend;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cloud.lc.group.shopping.controller.support.UserSupport;
import com.cloud.lc.group.shopping.dto.form.UserLoginForm;
import com.cloud.lc.group.shopping.dto.vo.UserDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.cloud.lc.group.shopping.annotation.NoLogin;
import com.cloud.lc.group.shopping.constant.WebConstant;
import com.cloud.lc.group.shopping.controller.support.SystemOprSupport;
import com.cloud.lc.group.shopping.dao.entity.SystemOpr;
import com.cloud.lc.group.shopping.dto.Result;
import com.cloud.lc.group.shopping.dto.form.OprUserLoginForm;
import com.cloud.lc.group.shopping.logger.Logger;
import com.cloud.lc.group.shopping.util.CookieWrap;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */

@RestController("LoginController")
@Api(value = "用户登录/退出", tags = { "用户登录/退出接口" })
@RequestMapping(value = WebConstant.SYSTEM_FRONTEND_PRIFIX)
public class LoginController extends BasicController {

    @Resource
    private UserSupport userSupport;

    @ApiOperation("用户登录")
    @PostMapping("/login")
    @NoLogin
    public Result login(@RequestBody UserLoginForm user, HttpServletRequest request, HttpServletResponse res) {
        Logger.info(JSON.toJSONString(user));
        UserDto userDto = userSupport.checkUserExist(user.getLoginId(), user.getPassword());
        if (userDto != null) {

            try {
                Map<String, String> cookiesKV = new HashMap<>();
                cookiesKV.put(WebConstant.COOKIE_NAME_SHOP_UID, userDto.getId().toString());

                cookiesKV.put(WebConstant.COOKIE_NAME_SHOP_UINFO,
                        URLEncoder.encode(JSON.toJSONString(userDto), WebConstant.GLOBAL_DEFAULT_CHARSET));
                setCookies(res,
                        new CookieWrap(cookiesKV, WebConstant.COOKIE_BASE_PATH, WebConstant.COOKIE_EXPIRED_SECOUND)); // 写入cookies
            } catch (UnsupportedEncodingException e) {
                logger.error("URLEncoder encode 用户信息异常 userInfo=" + JSON.toJSONString(userDto), e);
                return Result.fail("系统错误");
            }
            return Result.succ("登录成功");
        }
        return Result.fail(WebConstant.PASSWORD_ERROR, "用户名或密码错误");
    }

    @ApiOperation("用户退出")
    @GetMapping("/loginout")
    public Result loginout(HttpServletRequest req, HttpServletResponse res) {
        removeCookies(req, res, WebConstant.COOKIE_NAME_SHOP_UID, WebConstant.COOKIE_NAME_SHOP_UINFO);
        return Result.succ("ok");
    }

}