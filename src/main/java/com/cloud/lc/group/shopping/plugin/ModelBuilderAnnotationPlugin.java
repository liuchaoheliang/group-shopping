/*
 * Copyright (C) 2019 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.plugin;

import java.util.List;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.TopLevelClass;

public class ModelBuilderAnnotationPlugin extends PluginAdapter {

    @Override
    public boolean validate(List<String> warnings) {
        return true;
    }

    @Override
    public boolean modelBaseRecordClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        // Annotation
        topLevelClass.addImportedType(new FullyQualifiedJavaType("lombok.Builder")); //$NON-NLS-1$
        topLevelClass.addAnnotation("@Builder"); //$NON-NLS-1$
        return true;
    }
}
