/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.service;

import java.util.List;

import com.cloud.lc.group.shopping.base.service.BaseService;
import com.cloud.lc.group.shopping.dao.entity.Order;
import com.cloud.lc.group.shopping.dao.entity.OrderExample;

public interface OrderService extends BaseService<Order, OrderExample> {

    // public List<Order> getByConditions(OrderExample t);

}
