/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.support;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.cloud.lc.group.shopping.dao.entity.PayMethod;
import com.cloud.lc.group.shopping.dto.form.SavePayMethodForm;
import com.cloud.lc.group.shopping.dto.vo.PayMethodDto;
import com.cloud.lc.group.shopping.service.PayMethodService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.dao.entity.User;
import com.cloud.lc.group.shopping.dao.entity.UserExample;
import com.cloud.lc.group.shopping.dto.form.RegisterUserForm;
import com.cloud.lc.group.shopping.dto.form.SaveUserForm;
import com.cloud.lc.group.shopping.dto.vo.UserDto;
import com.cloud.lc.group.shopping.service.UserService;
import com.cloud.lc.group.shopping.util.XBeanUtil;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/11
 * @since 1.0.0
 */

@Component
public class PayMethodSupport {

    @Resource
    private PayMethodService payMethodService;

    public Page getByPage(Page reqPage) {
        Page<PayMethod> resPage = payMethodService.getByPage(reqPage);
        List<PayMethodDto> resContents =
                resPage.getResultsContent().stream().map(user -> domianToDto(user)).collect(Collectors.toList());
        reqPage.setTotalCount(resPage.getTotalCount());
        reqPage.setPageCount(resPage.getPageCount());
        reqPage.setResultsContent(resContents);
        return reqPage;
    }

    private PayMethodDto domianToDto(PayMethod payMethod) {
        PayMethodDto payMethodDto = new PayMethodDto();
        try {
            XBeanUtil.copyPropertiesIgnoresNull(payMethodDto, payMethod);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return payMethodDto;
    }

    public void save(SavePayMethodForm savePayMethodForm) throws InvocationTargetException, IllegalAccessException {
        PayMethod payMethod = new PayMethod();
        XBeanUtil.copyPropertiesIgnoresNull(payMethod, savePayMethodForm);
        if (savePayMethodForm.getId() != null) {
            payMethodService.updateById(payMethod);
        } else {
            payMethodService.insert(payMethod);
        }
    }

}