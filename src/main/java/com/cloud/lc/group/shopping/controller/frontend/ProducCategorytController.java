/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.frontend;

import javax.annotation.Resource;

import com.cloud.lc.group.shopping.annotation.NoLogin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cloud.lc.group.shopping.constant.WebConstant;
import com.cloud.lc.group.shopping.controller.support.ProductSupport;
import com.cloud.lc.group.shopping.dto.Result;
import com.cloud.lc.group.shopping.dto.form.SaveProductCategoryForm;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */

@RestController("ProducCategorytController")
@Api(value = "商品分类接口", tags = { "商品分类接口" })
@RequestMapping(value = WebConstant.SYSTEM_FRONTEND_PRIFIX + "/productCategory")
public class ProducCategorytController {

    @Resource
    private ProductSupport productSupport;

    @GetMapping(value = "/list")
    @ApiOperation("查询商品分类")
    @NoLogin
    public Result list(Long parentCategoryId) {
        return Result.succ(productSupport.getCategoryByParentId(parentCategoryId));
    }
}