/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.backend;

import com.cloud.lc.group.shopping.base.page.Order;
import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.base.page.PageFilter;
import com.cloud.lc.group.shopping.controller.support.MerchantSupport;
import com.cloud.lc.group.shopping.dto.form.QueryApplyForm;
import com.cloud.lc.group.shopping.dto.form.QueryMerchantForm;
import com.cloud.lc.group.shopping.dto.vo.SystemOprDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cloud.lc.group.shopping.constant.WebConstant;

import io.swagger.annotations.Api;

import java.util.Date;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */

@RestController
@Api(value = "商户相关接口", tags = { "商户相关接口" })
@RequestMapping(value = WebConstant.SYSTEM_BACKEND_PRIFIX + "/merchant")
public class MerchantController {

    @Autowired
    private MerchantSupport merchantSupport;

    @GetMapping(value = "/page")
    @ApiOperation("商户接入申请分页查询")
    public Page<SystemOprDto> getByPage(Page page, @ApiParam QueryMerchantForm queryMerchantForm) {
        if (page == null) {
            page = new Page();
        }

        if (queryMerchantForm.getPage() != null) {
            page.setPageNumber(queryMerchantForm.getPage());
        }
        if (queryMerchantForm.getRows() != null) {
            page.setPageSize(queryMerchantForm.getRows());
        }

        if (page.getPageFilter() == null) {
            PageFilter pageFilterDto = new PageFilter();
            // pageFilterDto.setStartTime(DateUtils.addDays(new Date(), -30));
            // pageFilterDto.setEndTime(new Date());
            page.setPageFilter(pageFilterDto);
        }
        page.getPageFilter().setFilterEntity(queryMerchantForm);

        // 排序
        Order order = new Order();
        order.setDirection(Order.Direction.desc);
        order.setProperty("create_time");
        page.setOrder(order);

        return merchantSupport.getMerchantByPage(page);
    }

}