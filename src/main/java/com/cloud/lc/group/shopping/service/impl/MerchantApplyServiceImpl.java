/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.cloud.lc.group.shopping.dao.entity.Order;
import org.springframework.stereotype.Service;

import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.dao.entity.MerchantApply;
import com.cloud.lc.group.shopping.dao.entity.MerchantApplyExample;
import com.cloud.lc.group.shopping.dao.mapper.MerchantApplyMapper;
import com.cloud.lc.group.shopping.service.MerchantApplyService;

@Service
public class MerchantApplyServiceImpl implements MerchantApplyService {

    @Resource
    private MerchantApplyMapper merchantApplyMapper;

    @Override
    public long insert(MerchantApply merchantApply) {
        merchantApplyMapper.insertSelective(merchantApply);
        return merchantApply.getId();
    }

    @Override
    public MerchantApply getById(Long id) {
        return merchantApplyMapper.selectByPrimaryKey(id);
    }

    @Override
    public void deleteById(Long id) {
        merchantApplyMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void updateById(MerchantApply merchantApply) {
        merchantApplyMapper.updateByPrimaryKeySelective(merchantApply);
    }

    @Override
    public List<MerchantApply> getByConditions(MerchantApplyExample merchantApply) {
        return merchantApplyMapper.selectByExample(merchantApply);
    }

    @Override
    public Page<MerchantApply> getByPage(Page<MerchantApply> page) {
        List<MerchantApply> list = merchantApplyMapper.getByPage(page);
        page.setResultsContent(list);
        return page;
    }
}
