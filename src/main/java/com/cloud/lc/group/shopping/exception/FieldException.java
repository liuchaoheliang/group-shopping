//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.cloud.lc.group.shopping.exception;

import java.util.Arrays;
import java.util.List;

public class FieldException extends SystemException {
    private static final long serialVersionUID = 253411714368648403L;
    private List<FieldExceptionItem> fieldErrors;

    public FieldException() {
    }

    public FieldException(String message, Throwable cause) {
        super(message, cause);
    }

    public FieldException(String message) {
        super(message);
    }

    public FieldException(FieldExceptionItem...items) {
        this.fieldErrors = Arrays.asList(items);
    }

    public FieldException(String message, FieldExceptionItem...items) {
        super(message);
        this.fieldErrors = Arrays.asList(items);
    }

    public FieldException(String message, Object[] args, Throwable cause) {
        super(message, args, cause);
    }

    public FieldException(String message, Object[] args) {
        super(message, args);
    }

    public FieldException(String message, Object[] args, FieldExceptionItem...items) {
        super(message, args);
        this.fieldErrors = Arrays.asList(items);
    }

    public FieldException(Throwable cause) {
        super(cause);
    }

    public FieldException(int errorCode, String message, Object[] args, Throwable cause) {
        super(errorCode, message, args, cause);
    }

    public FieldException(int errorCode, String message, Object[] args) {
        super(errorCode, message, args);
    }

    public FieldException(int errorCode, String message, Throwable cause) {
        super(errorCode, message, cause);
    }

    public FieldException(int errorCode, String message) {
        super(errorCode, message);
    }

    public List<FieldExceptionItem> getFieldErrors() {
        return this.fieldErrors;
    }

    public void setFieldErrors(List<FieldExceptionItem> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }
}
