/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping;

import com.cloud.lc.group.shopping.filter.CharsetFilter;
import com.cloud.lc.group.shopping.listener.ContextListener;
import com.cloud.lc.group.shopping.logger.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

import javax.servlet.DispatcherType;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.util.EnumSet;


/**
 * Hello world!
 *
 */
@SpringBootApplication
public class SpringbootApplication  extends  SpringBootServletInitializer{
	
	
	
	
    @Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(SpringbootApplication.class);
	}
    
    

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		Logger.info( "Application is starting !!!" );
		
        // 配置过滤器 + 过滤器路径
        servletContext.addFilter("timeFilter",new CharsetFilter()).addMappingForUrlPatterns( EnumSet.of(DispatcherType.REQUEST) , true , "/*" );
        
		// 配置监听器
        servletContext.addListener(new ContextListener());
		
		
	}



	public static void main( String[] args ){
    	SpringApplication.run(SpringbootApplication.class, args);
        Logger.info( "Application is running !!!" );
    }
}
