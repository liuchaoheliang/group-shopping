/*
 * Copyright (C) 2019 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.handler.vo;

import java.util.Map;

/**
 * 字段异常VO
 * 
 * @author LUYI374
 * @date 2017年2月17日
 * @since 1.0.0
 */
public class FieldExceptionVO extends ExceptionVO {

    /**
     * 字段异常信息<br>
     * 
     * KEY -> 字段名<br>
     * VAL -> 异常message<br>
     */
    private Map<String, String> fieldErrors;

    public Map<String, String> getFieldErrors() {
        return fieldErrors;
    }

    public void setFieldErrors(Map<String, String> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }
}