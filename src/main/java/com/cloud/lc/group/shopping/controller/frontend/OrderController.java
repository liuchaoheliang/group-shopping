/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.frontend;

import com.cloud.lc.group.shopping.annotation.NoLogin;
import com.cloud.lc.group.shopping.base.page.Order;
import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.base.page.PageFilter;
import com.cloud.lc.group.shopping.controller.support.OrderSupport;
import com.cloud.lc.group.shopping.dto.Result;
import com.cloud.lc.group.shopping.dto.form.CreateOrderForm;
import com.cloud.lc.group.shopping.dto.form.MerchantApplyForm;
import com.cloud.lc.group.shopping.dto.form.QueryOrderForm;
import com.cloud.lc.group.shopping.dto.form.QueryProductForm;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cloud.lc.group.shopping.constant.WebConstant;

import io.swagger.annotations.Api;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */

@RestController("OrderController")
@Api(value = "订单相关接口", tags = { "订单相关接口" })
@RequestMapping(value = WebConstant.SYSTEM_FRONTEND_PRIFIX + "/order")
public class OrderController extends BasicController {

    @Resource
    private OrderSupport orderSupport;

    @PostMapping("create")
    @ApiOperation("创建订单")
    public Result create(HttpServletRequest request, @RequestBody @ApiParam CreateOrderForm createOrderForm) {
        Long userId = getSessionUserId(request);
        createOrderForm.setUserId(userId);
        return Result.succ(orderSupport.creatOrder(createOrderForm));
    }

    @GetMapping(value = "/page")
    @ApiOperation("商品搜索分页查询")
    public Result getByPage(HttpServletRequest request, Page page, @ApiParam QueryOrderForm queryOrderForm) {
        Long userId = getSessionUserId(request);

        if (page == null) {
            page = new Page();
        }
        if (page.getPageFilter() == null) {
            PageFilter pageFilterDto = new PageFilter();
            pageFilterDto.setStartTime(DateUtils.addDays(new Date(), -30));
            pageFilterDto.setEndTime(new Date());
            pageFilterDto.setProperty("create_time");
            page.setPageFilter(pageFilterDto);
        }

        queryOrderForm.setUserId(userId);
        page.getPageFilter().setFilterEntity(queryOrderForm);

        // 排序
        Order order = new Order();
        order.setDirection(Order.Direction.desc);
        order.setProperty("create_time");
        page.setOrder(order);

        return Result.succ(orderSupport.getOderByPage(page));
    }
}