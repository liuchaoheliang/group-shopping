//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.cloud.lc.group.shopping.exception;

public class ForbiddenException extends SystemException {
    public ForbiddenException(String message) {
        super(message);
    }
}
