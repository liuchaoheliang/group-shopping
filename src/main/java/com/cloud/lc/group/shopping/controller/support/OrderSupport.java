/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.support;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.dao.entity.Order;
import com.cloud.lc.group.shopping.dao.entity.Product;
import com.cloud.lc.group.shopping.dto.form.CreateOrderForm;
import com.cloud.lc.group.shopping.dto.vo.MerchantBreifDto;
import com.cloud.lc.group.shopping.dto.vo.MerchantDto;
import com.cloud.lc.group.shopping.dto.vo.OrderDto;
import com.cloud.lc.group.shopping.dto.vo.ProductBreifDto;
import com.cloud.lc.group.shopping.dto.vo.ProductDto;
import com.cloud.lc.group.shopping.dto.vo.UserDto;
import com.cloud.lc.group.shopping.service.OrderService;
import com.cloud.lc.group.shopping.service.ProductService;
import org.springframework.stereotype.Component;

import com.cloud.lc.group.shopping.util.XBeanUtil;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/11
 * @since 1.0.0
 */

@Component
public class OrderSupport {

    @Resource
    private OrderService orderService;

    @Resource
    private ProductService productService;

    private OrderDto domianToDto(Order order) {
        OrderDto orderDto = new OrderDto();
        try {

            XBeanUtil.copyPropertiesIgnoresNull(orderDto, order);

            UserDto userDto = new UserDto();
            XBeanUtil.copyPropertiesIgnoresNull(userDto, order.getUser());
            orderDto.setUser(userDto);

            ProductBreifDto productDto = new ProductBreifDto();
            XBeanUtil.copyPropertiesIgnoresNull(productDto, order.getProduct());
            orderDto.setProduct(productDto);

            MerchantBreifDto merchantDto = new MerchantBreifDto();
            XBeanUtil.copyPropertiesIgnoresNull(merchantDto, order.getMerchant());
            orderDto.setMerchant(merchantDto);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return orderDto;
    }

    public Long creatOrder(CreateOrderForm createOrderForm) {
        Order order = new Order();
        try {
            createOrderForm.setPrice(createOrderForm.getPrice() * 100);
            createOrderForm.setTotalAmount(createOrderForm.getTotalAmount() * 100);
            XBeanUtil.copyPropertiesIgnoresNull(order, createOrderForm);

            Product product = productService.getById(createOrderForm.getProductId());
            if (product != null) {
                order.setMerchantId(product.getMerchantId());
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        // 默认直接支付成功
        order.setStatus(3);
        order.setPresentPoint(0);
        return orderService.insert(order);
    }

    public Page getOderByPage(Page reqPage) {
        Page<Order> resPage = orderService.getByPage(reqPage);
        List<OrderDto> resContents =
                resPage.getResultsContent().stream().map(order -> domianToDto(order)).collect(Collectors.toList());
        reqPage.setTotalCount(resPage.getTotalCount());
        reqPage.setPageCount(resPage.getPageCount());
        reqPage.setResultsContent(resContents);
        return reqPage;
    }
}