/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.base.mapper;

import java.util.List;

import com.cloud.lc.group.shopping.base.page.Page;

public interface BaseMapper<T> {
	
	public void insert(T object);
	
	public T getById(long id);
	
	public int updateById(T object);
	
	public int deleteById(long id);
	
	public List<T> getByConditions(T object);

    public List<T> getByPage(Page<T> page);
	
}