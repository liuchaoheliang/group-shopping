/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.frontend;

import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cloud.lc.group.shopping.annotation.NoLogin;
import com.cloud.lc.group.shopping.base.page.Order;
import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.base.page.PageFilter;
import com.cloud.lc.group.shopping.constant.WebConstant;
import com.cloud.lc.group.shopping.controller.support.ProductSupport;
import com.cloud.lc.group.shopping.dto.Result;
import com.cloud.lc.group.shopping.dto.form.QueryProductForm;
import com.cloud.lc.group.shopping.dto.form.SaveProductForm;
import com.cloud.lc.group.shopping.dto.vo.SystemOprDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */

@RestController("ProductController")
@Api(value = "商品相关接口", tags = { "商品相关接口" })
@RequestMapping(value = WebConstant.SYSTEM_FRONTEND_PRIFIX + "/product")
public class ProductController {

    @Resource
    private ProductSupport productSupport;

    @GetMapping(value = "/page")
    @ApiOperation("商品搜索分页查询")
    @NoLogin
    public Result getByPage(Page page, @ApiParam QueryProductForm queryProductForm) {
        if (page == null) {
            page = new Page();
        }
        if (page.getPageFilter() == null) {
            PageFilter pageFilterDto = new PageFilter();
            pageFilterDto.setStartTime(DateUtils.addDays(new Date(), -30));
            pageFilterDto.setEndTime(new Date());
            pageFilterDto.setProperty("create_time");
            page.setPageFilter(pageFilterDto);
        }

        page.getPageFilter().setFilterEntity(queryProductForm);

        // 排序
        Order order = new Order();
        order.setDirection(Order.Direction.desc);
        order.setProperty("create_time");
        page.setOrder(order);

        return Result.succ(productSupport.getProductByPage(page));
    }

    @GetMapping(value = "/detail")
    @ApiOperation("商品详情查询")
    @NoLogin
    public Result getByPage(Long productId) {
        return Result.succ(productSupport.productDetail(productId));
    }

}
