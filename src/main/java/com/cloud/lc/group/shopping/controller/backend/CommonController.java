/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.backend;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.cloud.lc.group.shopping.constant.WebConstant;
import com.cloud.lc.group.shopping.dto.Result;
import com.cloud.lc.group.shopping.logger.Logger;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */

@RestController
@RequestMapping(value = WebConstant.SYSTEM_BACKEND_PRIFIX + "/common")
public class CommonController {

    @Value("${upload.images.path}")
    private String uploadImagesPath;

    @Value("${image.address}")
    private String imageAddress;

    @PostMapping(value = "imageUpload")
    public @ResponseBody Result imageUpload(@RequestParam("file") MultipartFile file) {
        // 判断文件是否为空
        String fileName = "";
        String imagesPath = "";
        String filePath = "";
        JSONObject res = new JSONObject();
        if (!file.isEmpty()) {
            try {
                // 文件保存路径
                // String filePath = request.getSession().getServletContext().getRealPath("/") + "upload/"+
                // file.getOriginalFilename();
                UUID uuid = UUID.randomUUID();
                fileName =
                        uuid.toString() + file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("."));
                imagesPath = new SimpleDateFormat("yyyyMMdd").format(new Date()) + WebConstant.SYSTEM_FILE_SEPARATOR
                        + fileName;
                filePath = uploadImagesPath + WebConstant.SYSTEM_FILE_SEPARATOR + imagesPath;
                // 转存文件
                File path = new File(filePath);
                if (!path.exists()) {
                    new File(path.getParent()).mkdirs();
                    path.createNewFile();
                }
                file.transferTo(new File(filePath));
            } catch (Exception e) {
                Logger.error("上传图片异常 filePath=" + filePath + " fileName=" + fileName, e);
                e.printStackTrace();
                return Result.fail("图片上传失败");
            }
            res.put("link", imageAddress + "/" + imagesPath);
            res.put("name", fileName);
        }
        return Result.succ(res);
    }
}