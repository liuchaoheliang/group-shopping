/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.cloud.lc.group.shopping.dao.entity.Product;
import org.springframework.stereotype.Service;

import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.dao.entity.Order;
import com.cloud.lc.group.shopping.dao.entity.OrderExample;
import com.cloud.lc.group.shopping.dao.mapper.OrderMapper;
import com.cloud.lc.group.shopping.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderMapper orderMapper;

    @Override
    public long insert(Order order) {
        orderMapper.insertSelective(order);
        return order.getId();
    }

    @Override
    public Order getById(Long id) {
        return orderMapper.selectByPrimaryKey(id);
    }

    @Override
    public void deleteById(Long id) {
        orderMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void updateById(Order order) {
        orderMapper.updateByPrimaryKeySelective(order);
    }

    @Override
    public List<Order> getByConditions(OrderExample order) {
        return orderMapper.selectByExample(order);
    }

    @Override
    public Page<Order> getByPage(Page<Order> page) {
        List<Order> list = orderMapper.getByPage(page);
        page.setResultsContent(list);
        return page;
    }
}
