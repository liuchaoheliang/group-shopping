/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.frontend;

import com.cloud.lc.group.shopping.annotation.NoLogin;
import com.cloud.lc.group.shopping.controller.support.MerchantSupport;
import com.cloud.lc.group.shopping.dto.Result;
import com.cloud.lc.group.shopping.dto.form.MerchantApplyForm;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cloud.lc.group.shopping.constant.WebConstant;

import io.swagger.annotations.Api;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */

@RestController("MerchantApplyController")
@Api(value = "商户申请相关接口", tags = { "商户申请相关接口" })
@RequestMapping(value = WebConstant.SYSTEM_FRONTEND_PRIFIX + "/merchantApply")
public class MerchantApplyController {

    @Autowired
    private MerchantSupport merchantSupport;

    @PostMapping()
    @NoLogin
    @ApiOperation("商户接入申请")
    public Result apply(@RequestBody @ApiParam MerchantApplyForm merchantApplyForm) {
        merchantSupport.addMerchantAplly(merchantApplyForm);
        return Result.succ("申请已提交");
    }

}