/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.webconfig;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cloud.lc.group.shopping.plugin.StringToDateConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.GenericConverter;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.server.HandshakeInterceptor;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.cloud.lc.group.shopping.filter.CharsetFilter;
import com.cloud.lc.group.shopping.interceptor.CommonInterceptor;
import com.cloud.lc.group.shopping.servlet.MyServlet;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@EnableWebSocket
@EnableSwagger2
public class WebConfig extends WebMvcConfigurerAdapter {

	@Autowired
    private CommonInterceptor myInterceptor;

    /**
     * 数据json转换
     *
     * @return
     */
	@Bean
	public HttpMessageConverters fastJsonHttpMessageConverters() {
        FastJsonHttpMessageConverter fastJsonHttpMessageConverter = new FastJsonHttpMessageConverter();

        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);

        fastJsonHttpMessageConverter.setFastJsonConfig(fastJsonConfig);

        HttpMessageConverter<?> converter = fastJsonHttpMessageConverter;
        return new HttpMessageConverters(converter);
    }

	/**
	 * 自定义servlet
	 * @return
	 */
	@Bean
	public ServletRegistrationBean servletRegistrationBean() {
	    return new ServletRegistrationBean(new MyServlet(),"/myServlet");
	}

	/**
	 * 自定义filter
	 * @return
	 */
	@Bean
	public FilterRegistrationBean myFilter() {
	    FilterRegistrationBean registrationBean = new FilterRegistrationBean();

	    CharsetFilter timeFilter = new CharsetFilter();
	    registrationBean.setFilter(timeFilter);

	    //过滤器拦截路径配置
	    List<String> urls = new ArrayList();
	    urls.add("/*");

	    registrationBean.setUrlPatterns(urls);

	    return registrationBean;
	}

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new StringToDateConverter());
        super.addFormatters(registry);
    }

	/**
	 * 添加自定义拦截器
	 * @return
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		//添加拦截器，并制定拦截器路径
        registry.addInterceptor(myInterceptor).addPathPatterns("/backend/api/**", "/frontend/api/**");
	}

	/**
	 * 添加接口访问限制
	 * 粗粒度访问限制
	 * @return
	 */
	@Override
	public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("*/**").allowedOrigins("*/*");
	}

	@Bean
	public Docket accessToken() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("api")// 定义组
				.select() // 选择那些路径和 api 会生成 document
				.apis(RequestHandlerSelectors.basePackage("com.cloud.lc.group.shopping.controller")) // 拦截的包路径
				.paths(PathSelectors.regex("/*/.*"))// 拦截的接口路径
				.build() // 创建
				.apiInfo(apiInfo()); // 配置说明
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()//
		.title("团购网站")// 标题
		.description("基于web的团购网站设计")// 描述
		.termsOfServiceUrl("http://www.groupShopping.com")//
		.contact(new Contact("Dreamer", "http://www.teamGroup.com", "412792952@qq.com"))// 联系
		.version("1.0")// 版本
		.build();
		}

    private static class MyHandshakeInterceptor implements HandshakeInterceptor {
        @Override
        public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response,
                WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception {
            return true;
        }

        @Override
        public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler,
                Exception exception) {
        }
    }
}
