/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.base.service;

import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.dao.entity.Order;
import com.cloud.lc.group.shopping.dao.entity.OrderExample;

import java.util.List;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */
public interface BaseService<T, S> {

    public long  insert(T t);

    public T  getById(Long id);

    public void deleteById(Long id);

    public void updateById(T t);

    List<T> getByConditions(S s);

    public Page<T> getByPage(Page<T> page);

}
