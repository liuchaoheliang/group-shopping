/*
 * Copyright (C) 2019 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.plugin;

import java.util.List;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.JavaVisibility;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.TopLevelClass;

public class FluentBuilderMethodsPlugin extends PluginAdapter {

    @Override
    public boolean validate(List<String> warnings) {
        return true;
    }

    @Override
    public boolean modelSetterMethodGenerated(Method method, TopLevelClass topLevelClass,
            IntrospectedColumn introspectedColumn, IntrospectedTable introspectedTable, ModelClassType modelClassType) {

        Method fluentMethod = new Method();
        fluentMethod.setVisibility(JavaVisibility.PUBLIC);
        fluentMethod.setReturnType(topLevelClass.getType());
        fluentMethod.setName("with" + method.getName().substring(3)); //$NON-NLS-1$
        fluentMethod.getParameters().addAll(method.getParameters());

        context.getCommentGenerator().addGeneralMethodComment(fluentMethod, introspectedTable);
        StringBuilder sb = new StringBuilder().append("this.") //$NON-NLS-1$
                .append(method.getName()).append('(').append(introspectedColumn.getJavaProperty()).append(");"); //$NON-NLS-1$
        fluentMethod.addBodyLine(sb.toString()); // $NON-NLS-1$
        fluentMethod.addBodyLine("return this;"); //$NON-NLS-1$

        topLevelClass.addMethod(fluentMethod);

        return super.modelSetterMethodGenerated(method, topLevelClass, introspectedColumn, introspectedTable,
                modelClassType);
    }
}
