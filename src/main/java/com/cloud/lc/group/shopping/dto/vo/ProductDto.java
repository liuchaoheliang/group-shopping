/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.dto.vo;

import com.cloud.lc.group.shopping.annotation.IgnoreCopy;

import java.util.Date;

public class ProductDto {
    private Long id;

    private Date createTime;

    private Date updateTime;

    private Long categoryId;

    private Long merchantId;

    private String fullName;

    private String shortName;

    private String spellCode;

    private Integer price;

    private Integer marketPrice;

    private Integer store;

    private Integer sellCount;

    private Integer status;

    private String baseImageUrl;

    private Integer presentPoint;

    private String detailHtml;

    @IgnoreCopy
    private MerchantBreifDto merchant;

    public MerchantBreifDto getMerchant() {
        return merchant;
    }

    public void setMerchant(MerchantBreifDto merchant) {
        this.merchant = merchant;
    }

    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column
     * basic_product.id
     *
     * @param id the value for basic_product.id
     *
     * @mbggenerated
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column
     * basic_product.create_time
     *
     * @return the value of basic_product.create_time
     *
     * @mbggenerated
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column
     * basic_product.create_time
     *
     * @param createTime the value for basic_product.create_time
     *
     * @mbggenerated
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column
     * basic_product.update_time
     *
     * @return the value of basic_product.update_time
     *
     * @mbggenerated
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column
     * basic_product.update_time
     *
     * @param updateTime the value for basic_product.update_time
     *
     * @mbggenerated
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column
     * basic_product.category_id
     *
     * @return the value of basic_product.category_id
     *
     * @mbggenerated
     */
    public Long getCategoryId() {
        return categoryId;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column
     * basic_product.category_id
     *
     * @param categoryId the value for basic_product.category_id
     *
     * @mbggenerated
     */
    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column
     * basic_product.merchant_id
     *
     * @return the value of basic_product.merchant_id
     *
     * @mbggenerated
     */
    public Long getMerchantId() {
        return merchantId;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column
     * basic_product.merchant_id
     *
     * @param merchantId the value for basic_product.merchant_id
     *
     * @mbggenerated
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column
     * basic_product.full_name
     *
     * @return the value of basic_product.full_name
     *
     * @mbggenerated
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column
     * basic_product.full_name
     *
     * @param fullName the value for basic_product.full_name
     *
     * @mbggenerated
     */
    public void setFullName(String fullName) {
        this.fullName = fullName == null ? null : fullName.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column
     * basic_product.short_name
     *
     * @return the value of basic_product.short_name
     *
     * @mbggenerated
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column
     * basic_product.short_name
     *
     * @param shortName the value for basic_product.short_name
     *
     * @mbggenerated
     */
    public void setShortName(String shortName) {
        this.shortName = shortName == null ? null : shortName.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column
     * basic_product.spell_code
     *
     * @return the value of basic_product.spell_code
     *
     * @mbggenerated
     */
    public String getSpellCode() {
        return spellCode;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column
     * basic_product.spell_code
     *
     * @param spellCode the value for basic_product.spell_code
     *
     * @mbggenerated
     */
    public void setSpellCode(String spellCode) {
        this.spellCode = spellCode == null ? null : spellCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column
     * basic_product.price
     *
     * @return the value of basic_product.price
     *
     * @mbggenerated
     */
    public Integer getPrice() {
        return price;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column
     * basic_product.price
     *
     * @param price the value for basic_product.price
     *
     * @mbggenerated
     */
    public void setPrice(Integer price) {
        this.price = price;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column
     * basic_product.market_price
     *
     * @return the value of basic_product.market_price
     *
     * @mbggenerated
     */
    public Integer getMarketPrice() {
        return marketPrice;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column
     * basic_product.market_price
     *
     * @param marketPrice the value for basic_product.market_price
     *
     * @mbggenerated
     */
    public void setMarketPrice(Integer marketPrice) {
        this.marketPrice = marketPrice;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column
     * basic_product.store
     *
     * @return the value of basic_product.store
     *
     * @mbggenerated
     */
    public Integer getStore() {
        return store;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column
     * basic_product.store
     *
     * @param store the value for basic_product.store
     *
     * @mbggenerated
     */
    public void setStore(Integer store) {
        this.store = store;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column
     * basic_product.sell_count
     *
     * @return the value of basic_product.sell_count
     *
     * @mbggenerated
     */
    public Integer getSellCount() {
        return sellCount;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column
     * basic_product.sell_count
     *
     * @param sellCount the value for basic_product.sell_count
     *
     * @mbggenerated
     */
    public void setSellCount(Integer sellCount) {
        this.sellCount = sellCount;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column
     * basic_product.status
     *
     * @return the value of basic_product.status
     *
     * @mbggenerated
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column
     * basic_product.status
     *
     * @param status the value for basic_product.status
     *
     * @mbggenerated
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column
     * basic_product.base_image_url
     *
     * @return the value of basic_product.base_image_url
     *
     * @mbggenerated
     */
    public String getBaseImageUrl() {
        return baseImageUrl;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column
     * basic_product.base_image_url
     *
     * @param baseImageUrl the value for basic_product.base_image_url
     *
     * @mbggenerated
     */
    public void setBaseImageUrl(String baseImageUrl) {
        this.baseImageUrl = baseImageUrl == null ? null : baseImageUrl.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column
     * basic_product.present_point
     *
     * @return the value of basic_product.present_point
     *
     * @mbggenerated
     */
    public Integer getPresentPoint() {
        return presentPoint;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column
     * basic_product.present_point
     *
     * @param presentPoint the value for basic_product.present_point
     *
     * @mbggenerated
     */
    public void setPresentPoint(Integer presentPoint) {
        this.presentPoint = presentPoint;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column
     * basic_product.detail_html
     *
     * @return the value of basic_product.detail_html
     *
     * @mbggenerated
     */
    public String getDetailHtml() {
        return detailHtml;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column
     * basic_product.detail_html
     *
     * @param detailHtml the value for basic_product.detail_html
     *
     * @mbggenerated
     */
    public void setDetailHtml(String detailHtml) {
        this.detailHtml = detailHtml == null ? null : detailHtml.trim();
    }
}