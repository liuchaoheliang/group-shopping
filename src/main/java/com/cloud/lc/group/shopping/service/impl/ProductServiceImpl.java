/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.dao.entity.Product;
import com.cloud.lc.group.shopping.dao.entity.ProductExample;
import com.cloud.lc.group.shopping.dao.mapper.ProductMapper;
import com.cloud.lc.group.shopping.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

    @Resource
    private ProductMapper productMapper;

    @Override
    public long insert(Product product) {
        productMapper.insertSelective(product);
        return product.getId();
    }

    @Override
    public Product getById(Long id) {
        return productMapper.selectByPrimaryKey(id);
    }

    @Override
    public void deleteById(Long id) {
        productMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void updateById(Product product) {
        productMapper.updateByPrimaryKeySelective(product);
    }

    @Override
    public List<Product> getByConditions(ProductExample product) {
        return productMapper.selectByExample(product);
    }

    @Override
    public Page<Product> getByPage(Page<Product> page) {
        List<Product> list = productMapper.getByPage(page);
        page.setResultsContent(list);
        return page;
    }
}
