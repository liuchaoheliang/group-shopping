/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.backend;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import com.cloud.lc.group.shopping.dao.entity.SystemOpr;
import com.cloud.lc.group.shopping.dto.Result;
import com.cloud.lc.group.shopping.dto.form.SaveOprUserForm;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cloud.lc.group.shopping.base.page.Order;
import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.base.page.PageFilter;
import com.cloud.lc.group.shopping.constant.WebConstant;
import com.cloud.lc.group.shopping.controller.support.SystemOprSupport;
import com.cloud.lc.group.shopping.dto.form.QueryOprUserForm;
import com.cloud.lc.group.shopping.dto.vo.SystemOprDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.servlet.http.HttpServletRequest;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */

@RestController
@Api(value = "系统操作员相关接口", tags = { "系统操作员相关接口接口" })
@RequestMapping(value = WebConstant.SYSTEM_BACKEND_PRIFIX + "/systemOpr")
public class SystemOprController extends BasicController {

    @Autowired
    private SystemOprSupport systemOprSupport;


    @GetMapping("/page")
    @ApiOperation("系统用户分页查询")
    public Page<SystemOprDto> getByPage(Page page, @ApiParam QueryOprUserForm queryOprUserForm) {

        if (page == null) {
            page = new Page();
        }
        if (page.getPageFilter() == null) {
            PageFilter pageFilterDto = new PageFilter();
            // pageFilterDto.setStartTime(DateUtils.addDays(new Date(), -30));
            // pageFilterDto.setEndTime(new Date());
            page.setPageFilter(pageFilterDto);
        }
        if (!isAdmin()) {
            queryOprUserForm.setMerchantId(getMerchantId());
        }
        page.getPageFilter().setFilterEntity(queryOprUserForm);

        // 排序
        Order order = new Order();
        order.setDirection(Order.Direction.desc);
        order.setProperty("create_time");
        page.setOrder(order);

        return systemOprSupport.getByPage(page);
    }

    @PostMapping("/save")
    @ApiOperation("添加系统用户")
    public Result save(HttpServletRequest request, @RequestBody @ApiParam SaveOprUserForm saveOprUserForm)
            throws InvocationTargetException, IllegalAccessException {
        SystemOpr user = getLoginSysUser();
        saveOprUserForm.setMerchantId(user.getMerchantId());
        systemOprSupport.save(saveOprUserForm);
        return Result.succ(true);
    }

    @PostMapping("/update")
    @ApiOperation("修改系统用户")
    public Result update(@RequestBody @ApiParam SaveOprUserForm saveOprUserForm)
            throws InvocationTargetException, IllegalAccessException {
        systemOprSupport.save(saveOprUserForm);
        return Result.succ(true);
    }
}