/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.service;

import java.util.List;

import com.cloud.lc.group.shopping.base.service.BaseService;
import com.cloud.lc.group.shopping.dao.entity.User;
import com.cloud.lc.group.shopping.dao.entity.UserExample;

public interface UserService extends BaseService<User, UserExample> {

}
