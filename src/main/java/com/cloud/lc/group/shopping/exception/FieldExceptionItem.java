//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.cloud.lc.group.shopping.exception;

public class FieldExceptionItem {
    private String field;
    private String message;
    private Object[] args;

    public FieldExceptionItem(String field, String message, Object...args) {
        this.field = field;
        this.message = message;
        this.args = args;
    }

    public String getField() {
        return this.field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object[] getArgs() {
        return this.args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }
}
