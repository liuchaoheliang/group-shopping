package com.cloud.lc.group.shopping.base.page;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.ModelAttribute;


/**
 * 分页面过滤条件
 * @author
 *
 */
public class PageFilter<T> implements Serializable {
	
	private T filterEntity;//筛选条件对象

    // 默认匹配创建时间属性
    private String property = "create_time";// 属性

	private Date startTime;//开始时间

	private Date endTime;//结束时间
	
	public T getFilterEntity() {
		return filterEntity;
	}

	public void setFilterEntity(T filterEntity) {
		this.filterEntity = filterEntity;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
}

