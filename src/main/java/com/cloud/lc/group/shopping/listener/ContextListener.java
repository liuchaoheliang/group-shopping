/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.listener;

import com.cloud.lc.group.shopping.logger.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ContextListener implements ServletContextListener {

	public void contextDestroyed(ServletContextEvent arg0) {
		Logger.info("容器监听已销毁");
	}

	public void contextInitialized(ServletContextEvent arg0) {
		Logger.info("容器监听正在初始化...");
	}

}
