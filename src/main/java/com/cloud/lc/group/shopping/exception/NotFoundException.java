package com.cloud.lc.group.shopping.exception;

import com.cloud.lc.group.shopping.exception.SystemException;

public class NotFoundException extends SystemException {
    public NotFoundException(String message) {
        super(message);
    }
}
