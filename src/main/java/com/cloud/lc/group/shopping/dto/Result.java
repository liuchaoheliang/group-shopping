/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.dto;

import com.alibaba.fastjson.JSON;
import com.cloud.lc.group.shopping.constant.WebConstant;

/**
 * 业务执行返回体
 * @author 刘超
 */
public class Result {
	
	/**
     * 网络响应码
     */
	private Integer code;
	
	/**
     * msg响应消息
     */
	private String msg;
	
	/**
     * 请求响应对象
     */
	private Object data;

	public Result(){
		
	}

    public Result(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static Result succ(Object respObject) {
        return new Result(WebConstant.RESP_CODE_SUCCESS, null, respObject);
	}
	
	public static Result succ( String respMsg, Object respObject ){
        return new Result(WebConstant.RESP_CODE_SUCCESS, respMsg, respObject);
	}

	public static Result fail( String respMsg ){
        return new Result(WebConstant.RESP_CODE_FAIL, respMsg, null);
	}

    public static Result fail(Integer code, String respMsg) {
        return new Result(code, respMsg, null);
    }
	
	public Boolean isSucc(){
        return WebConstant.RESP_CODE_SUCCESS == this.code;
	}
	
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}
	
}
