/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.group.shopping.controller.support;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.cloud.lc.group.shopping.dao.entity.Merchant;
import com.cloud.lc.group.shopping.dao.entity.MerchantApply;
import com.cloud.lc.group.shopping.dao.entity.Order;
import com.cloud.lc.group.shopping.dao.entity.Product;
import com.cloud.lc.group.shopping.dto.form.MerchantApplyForm;
import com.cloud.lc.group.shopping.dto.form.MerchantForm;
import com.cloud.lc.group.shopping.dto.vo.MerchantApplyDto;
import com.cloud.lc.group.shopping.dto.vo.MerchantDto;
import com.cloud.lc.group.shopping.dto.vo.OrderDto;
import com.cloud.lc.group.shopping.dto.vo.ProductDto;
import com.cloud.lc.group.shopping.dto.vo.UserDto;
import com.cloud.lc.group.shopping.service.MerchantApplyService;
import com.cloud.lc.group.shopping.service.MerchantService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import com.cloud.lc.group.shopping.base.page.Page;
import com.cloud.lc.group.shopping.dao.entity.SystemOpr;
import com.cloud.lc.group.shopping.dto.vo.SystemOprDto;
import com.cloud.lc.group.shopping.service.SystemOprService;
import com.cloud.lc.group.shopping.util.XBeanUtil;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2018/9/11
 * @since 1.0.0
 */

@Component
public class MerchantSupport {

    @Resource
    private MerchantService merchantService;

    @Resource
    private MerchantApplyService merchantApplyService;

    public MerchantApplyDto getApplyById(Long id) {
        MerchantApplyDto merchantApplyDto = null;
        MerchantApply merchantApply = merchantApplyService.getById(id);
        if (merchantApply != null) {
            merchantApplyDto = domianToDto(merchantApply);
        }
        return merchantApplyDto;
    }

    public void updateApplyById(Long id, Integer status) {
        MerchantApply merchantApply = new MerchantApply();
        merchantApply.setId(id);
        merchantApply.setStatus(status);
        merchantApplyService.updateById(merchantApply);
    }

    /**
     * 添加商户申请
     * 
     * @param merchantApplyForm
     */
    public void addMerchantAplly(MerchantApplyForm merchantApplyForm) {
        MerchantApply apply = new MerchantApply();
        try {
            XBeanUtil.copyPropertiesIgnoresNull(apply, merchantApplyForm);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        // 默认待审核
        apply.setStatus(0);
        merchantApplyService.insert(apply);
    }

    /**
     * 添加商户申请
     *
     * @param merchantForm
     */
    public void addMerchant(MerchantForm merchantForm) {
        Merchant merchant = new Merchant();
        try {
            XBeanUtil.copyPropertiesIgnoresNull(merchant, merchantForm);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        // 默认启用
        merchant.setStatus(0);
        merchantService.insert(merchant);
    }

    /**
     * 分页查询商户申请
     * 
     * @param reqPage
     * @return
     */
    public Page getApplyByPage(Page reqPage) {
        Page<MerchantApply> resPage = merchantApplyService.getByPage(reqPage);
        List<MerchantApplyDto> resContents = resPage.getResultsContent().stream()
                .map(systemOpr -> domianToDto(systemOpr)).collect(Collectors.toList());
        reqPage.setTotalCount(resPage.getTotalCount());
        reqPage.setPageCount(resPage.getPageCount());
        reqPage.setResultsContent(resContents);
        return reqPage;
    }

    private MerchantApplyDto domianToDto(MerchantApply merchantApply) {
        MerchantApplyDto merchantApplyDto = new MerchantApplyDto();
        try {
            XBeanUtil.copyPropertiesIgnoresNull(merchantApplyDto, merchantApply);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return merchantApplyDto;
    }

    /**
     * 分页查询商户信息
     * 
     * @param reqPage
     * @return
     */
    public Page getMerchantByPage(Page reqPage) {
        Page<Merchant> resPage = merchantService.getByPage(reqPage);
        List<MerchantDto> resContents = resPage.getResultsContent().stream().map(merchant -> domianToDto(merchant))
                .collect(Collectors.toList());
        reqPage.setTotalCount(resPage.getTotalCount());
        reqPage.setPageCount(resPage.getPageCount());
        reqPage.setResultsContent(resContents);
        return reqPage;
    }

    private MerchantDto domianToDto(Merchant merchant) {
        MerchantDto merchantDto = new MerchantDto();
        try {
            XBeanUtil.copyPropertiesIgnoresNull(merchantDto, merchant);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return merchantDto;
    }

}