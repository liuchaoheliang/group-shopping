<!DOCTYPE html>
<html>
<head lang="zh">
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/css/bootstrap-theme.min.css"/>
    <link rel="stylesheet" href="/css/toastr.css"/>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/BootstrapMenu.min.js"></script>
    <script src="/js/toastr.js"></script>
    <#include "/lib/my_test.ftl">
    <style type="text/css">
        #msg {
            height:100%;
            width:80%;
            overflow-y: auto;
            border-left: 1px solid gray;
        }
        #users {
            height:100%;
            width:15%;
            overflow-y: auto;
            float:left;
            padding: 7px;
            text-align: center;
        }
        #user-detail{
            color: #0a40ef;
        }

        #userName {
            width: 200px;
        }

        #logout-btn {
            display: none;
        }
        .well {
            height:400px;
            padding: 0px;
        }

        a {
            padding: 0px 15px;
        }
    </style>
<title>TIM在线聊天室</title>
</head>
<body>
<div class="container">
    <div class="page-header">
        <div class="input-group">
            <input id="userName" type="text" class="form-control" name="userName" placeholder="输入您的用户名"/>
            <button class="btn btn-default" type="button" id="connection-btn">建立连接</button>&nbsp;&nbsp;
            <button class="btn btn-danger" id="logout-btn">退出聊天</button>
            <button id="notify_btn" style="display:none" type="button" class="btn btn-default btn-sm">
                <span class="glyphicon glyphicon-bell"></span> 打开浏览器通知
            </button>
        </div>
    </div>

    <div id="connect_success" class="progress" style="display:none">
        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuemax="100" style="width: 100%;">
            已建立连接...
        </div>
    </div>
    <div id="connect_fail" class="progress" style="display:none">
        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuemax="100" style="width: 100%;">
            已断开连接...
        </div>
    </div>

    <div class="well" >
        <div id="users">
            <p>在线列表</p>
            <ul id = "user-detail" class="nav nav-pills nav-stacked">

            </ul>
        </div>
        <div id="msg"></div>
    </div>


    <div class="col-lg">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="发送信息..." id="message"/>
            <span class="input-group-btn"> <button class="btn btn-default" type="button" id="send" disabled="disabled">发送</button> </span>
        </div>
        <!-- /input-group -->
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->
<script type="text/javascript">
var uname;

$(function() {
    var websocket;
    $("#connection-btn").bind("click",
    function() {
        var userName = $("#userName").val();
        if (userName == null || userName == "") {
            toastr.error("请输入您的用户名");
            return;
        }
        connection(userName);
    });

    function connection(userName) {
        var host = window.location.host;
        if ('WebSocket' in window) {
            websocket = new WebSocket("ws://" + host + "/webSocketServer/" + userName);
        } else if ('MozWebSocket' in window) {
            websocket = new MozWebSocket("ws://" + host + "/webSocketServer/" + userName);
        }
        websocket.onopen = function(evnt) {
        	uname = userName;
        	toastr.success("链接成功!");
            $("#connect_success").show();
            $("#connect_fail").hide();
            $("#send").prop("disabled", "");
            $("#connection-btn").prop("disabled", "disabled");
            $("#logout-btn").show();
        };
        websocket.onmessage = function(evnt) {
        	recivedMsg(JSON.parse(evnt.data))
            $("#msg").scrollTop($("#msg")[0].scrollHeight);

        };
        websocket.onerror = function(evnt) {
            $("#connect_success").hide();
            $("#connect_fail").show(); 
            toastr.warning("运行出现错误");
        };
        websocket.onclose = function(evnt) {
        	toastr.warning("链接断开!");
            $("#connect_success").hide();
            $("#connect_fail").show();           
            $("#send").prop("disabled", "disabled");
            $("#connection-btn").prop("disabled", "");
            $("#logout-btn").hide();
        }
    }

    function send() {
        if (websocket != null) {
            var $message = $("#message");
            var inputMsg = $message.val();
            if (inputMsg == null || inputMsg == "") {
                return;
            }
            var data = {type:1,text:inputMsg};
            websocket.send(JSON.stringify(data));
            $message.val("");
        } else {
            toastr.warning('未与服务器链接');
        }
    }

    function refreshUserList(){
        var data = {type:3};
         websocket.send(JSON.stringify(data));
    }

    function recivedMsg(message){
        //系统消息
        if(message.msgType == 0){
            toastr.info(message.text);
            $("#msg").append("<br/><span>【系统提示】 " + message.text + "...</span>");
            notify();
        }
        //群发消息
        else if (message.msgType == 1){
            toastr.info(message.fromUser + ":" + message.text);
            $("#msg").append("<br/><span>【"+message.fromUser+"】" + message.createTime + "</span><br/><span>&nbsp;&nbsp;"+message.text+"</span>");
            if(message.fromUser != uname){
                notify();
            }
        }

        else if (message.msgType == 2){
            //TODO :
        }

        else if (message.msgType == 3){
            var users = JSON.parse(message.text);
            $("#user-detail").html("");
            for(u of users){
                $("#user-detail").append("<li><span>"+u+"</span></li>");
            }
        }
    }

    $('#send').bind('click',
    function() {
        send();
    });

    $(document).on("keypress",
    function(event) {
        if (event.keyCode == "13") {
            send();
        }
    });

    $("#logout-btn").on("click",
    function() {
        websocket.close(); //关闭TCP连接
    });
}); 
var menu = new BootstrapMenu('#msg', {
  actions: [{
      name: '清除记录',
      onClick: function() {
        $("#msg").html("");
      }
    }, {
      name: '退出聊天',
      onClick: function() {
        $("#logout-btn").trigger("click");
      }
    }]
});


function notify(){
	if(window.Notification && Notification.permission !== "denied") {
		Notification.requestPermission(function(status) {
			var n = new Notification('TIM在线聊天', { body: '您有一条新消息！' });
			n.onshow = function () {
  				setTimeout(n.close.bind(n), 5000);
			}
		});
	}
}

$("#notify_btn").on('click',function(){
	Notification.requestPermission(function (status) {
	  if (status === "granted") {
	    var n = new Notification("欢迎进去TIM在线聊天室");
	  }
	});
});

String.prototype.startWith=function(str){
  var reg=new RegExp("^"+str);
  return reg.test(this);
}

</script>
</body>
</html>