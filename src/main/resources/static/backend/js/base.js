//主机服务地址
var $base = "http://localhost:8880/backend/api/";

// 常量信息
var $cookie_uid_key = 'mgmt_opr_id';
var $cookie_token_key = 'mgmt_opr_token';
var $cookie_u_info_key = 'mgmt_opr_info';

//API地址访问列表
// var get_image_code_api = $base + "common/get_image_code.api";
var login_api = $base + "login";
var logout_api = $base + 'loginout';
var file_upload_api = $base + "common/imageUpload";

var query_product_category_by_cond_api = $base + "productCategory/list";
var save_product_category_api = $base + "productCategory/save";
var delete_product_category_by_id_api = $base + "productCategory/delete";

var save_product_api = $base + "product/save";
var query_product_by_page = $base + "product/page";
var query_product_by_id = $base + "product/detail";
var aduit_product_by_id = $base + "product/aduit";

var query_opr_by_page = $base + "systemOpr/page";
var save_opr_api = $base + "systemOpr/save";
var query_user_by_page = $base + "user/page";
var save_user_api = $base + "user/save";

var query_pay_method_by_page = $base + "payMethod/page";
var save_pay_method_api = $base + "payMethod/save";

var query_order_by_page = $base + "order/page";

var query_merchant_by_page = $base + "merchant/page";
var query_merchant_apply_by_page = $base + "merchantApply/page";
var aduit_merchant_apply_api = $base + "merchantApply/aduit";



//页面相对路径
var $main_index_location = '/backend/web/index.html';
var $login_index_location = '/backend/login.html';

// 登录态获判断
function tokenVerify() {
    var curPath = window.location.pathname;
    var uid = $.cookie($cookie_uid_key);
    var token = $.cookie($cookie_token_key);
    if (!uid || uid == 'null') {
        if (curPath != $login_index_location) {
            // 能判断当前未登录 未登录重定向到登录页面
            window.parent.location = $login_index_location;
        }
    } else {
        // 可以确认当前cookies信息有，但是无法保证cookies是正常的还是人为的
        // 由于后台安全性保证，可以当成前端有相应cookies则认为已经登录（页面级别表现）
        if (curPath == $login_index_location) { // 当前已经登录 已登录不再允许访问登录页面
            window.parent.location = $main_index_location;
        }
    }
}



function getUserInfo() {
    var userInfoJsonStr = $.cookie($cookie_u_info_key);
    if (!userInfoJsonStr || userInfoJsonStr == 'null') {
        logout();
    }
    return JSON.parse(userInfoJsonStr);
}

function isAdmin(){
    return getUserInfo().type == 1 ? true : false;
}

/*
登出动作
 */
function logout() {
    ajax(logout_api, 'get', function (data) {
        deleteTokenCookies();
        window.parent.location = $login_index_location;
    })
}

/*
* ajax封装方法 url  postData succCallback errorCallback type (默认post)  dataType(默认json)
* url <必须>
* succCallback <必须> 不然函数就没有意义
* type default post
*/
function ajax(url, type, reqJsonObj, succCallback, errorCallback) {

    if (typeof type == 'function') { // 未传入type&reqJsonObj
        succCallback = type;
        errorCallback = reqJsonObj;
        reqJsonObj = null;
    } else if (typeof type == 'object') { // 未传入type 传入reqJsonObj
        errorCallback = succCallback;
        succCallback = reqJsonObj;
        reqJsonObj = type;
        type = null;
    } else { // typeof type = 'string' 代表type传入了 可能存在reqJsonObj没有传入
        if (typeof reqJsonObj == 'function') { // reqJsonObj没有传入
            errorCallback = succCallback;
            succCallback = reqJsonObj;
            reqJsonObj = null;
        }
    }
    type = type || 'post';
    var reqJson = reqJsonObj ? JSON.stringify(reqJsonObj) : null;
    $.ajax({
        type: type,
        url: url,
        data: reqJson,
        dataType: 'json',
        contentType: 'application/json; charset=UTF-8',
        beforeSend: function () {//开始loading

        },
        success: function (result) {
            console.log(JSON.stringify(result));
            if (result.code == 200) {
                if (succCallback) {
                    succCallback(result.data);
                }
            }else {
                if (errorCallback) {
                    errorCallback(result);
                }
            }
        },
        error: function (error) {
            alert("服务繁忙请稍后再试...");
            console.log(JSON.stringify(error));
        },
        complete: function () {//结束loading

        }
    });
}

tokenVerify();

function deleteTokenCookies() {
    $.cookie($cookie_u_info_key, null, {path: '/'});
    $.cookie($cookie_token_key, null, {path: '/'});
    $.cookie($cookie_uid_key, null, {path: '/'});
}

//获取事件触发控件
function getEventElem(event) {
    return event.srcElement || event.target;
}

function $alert(messager,title){
    $.messager.alert((title || "提示"),messager);
}

/**
 * 获取日期对象
 */
function getDate(d) {
    var rd = null;
    if (d != null) {
        if (typeof d == 'object') {
            rd = d;
        } else if (typeof d == 'number') {
            rd = new Date(d);
        } else if (typeof d == 'string') {
            rd = new Date(Date.parse(d.replace(/-/g, "/")));
        }
    } else {
        rd = new Date();
    }
    var time = {};
    time.year = rd.getFullYear();
    time.month = rd.getMonth() + 1;
    time.date = rd.getDate();
    time.day = rd.getDay();
    time.hour = rd.getHours();
    time.minute = rd.getMinutes();
    time.second = rd.getSeconds();
    time.millisecond = rd.getMilliseconds();
    time.longTime = rd.getTime();

    time.年 = time.year;
    time.月 = time.month < 10 ? '0' + time.month : time.month;
    time.日 = time.date < 10 ? '0' + time.date : time.date;
    time.星期 = ['日', '一', '二', '三', '四', '五', '六'][time.day];
    time.weekDay = ['1', '2', '3', '4', '5', '6', '7'][time.day];
    time.时 = time.hour < 10 ? '0' + time.hour : time.hour;
    time.分 = time.minute < 10 ? '0' + time.minute : time.minute;
    time.秒 = time.second < 10 ? '0' + time.second : time.second;
    time.毫秒 = time.millisecond < 10 ? '00' + time.millisecond : (time.millisecond < 100 ? '0' + time.millisecond : time.millisecond);

    time.时间信息0 = time.年 + "-" + time.月 + "-" + time.日 + " " + time.时 + ":" + time.分 + ":" + time.秒 + "." + time.毫秒;
    time.时间信息1 = time.年 + "-" + time.月 + "-" + time.日 + " " + time.时 + ":" + time.分 + ":" + time.秒;
    time.时间信息2 = time.年 + "-" + time.月 + "-" + time.日;
    time.时间信息3 = time.时 + ":" + time.分 + ":" + time.秒;
    return time;
}

//重新easyUI日期格式化函数
$.fn.datebox.defaults.formatter = function(date){
	return getDate(date).时间信息2;
}

$.fn.datetimebox.defaults.formatter = function(date){
	return getDate(date).时间信息1;
}

//默认规则
$.extend($.fn.validatebox.defaults.rules, {
    minLength: {
        validator: function(value, param){
            return value.length >= param[0];
        },
        message: '最少限制{0}个字符.'
    },
    isDate : {
    	validator: function(value, param){
    		var reg = /^[1-9]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])\s+(20|21|22|23|[0-1]\d):[0-5]\d:[0-5]\d$/;
    		var regExp = new RegExp(reg);
            return regExp.test(value);
        },
        message: '日期格式不正确.'
    }
});


//强制保留2位小数，如：2，会在2后面补上00.即2.00
function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return false;
    }
    var f = Math.round(x*100)/100;
    var s = f.toString();
    var rs = s.indexOf('.');
    if (rs < 0) {
        rs = s.length;
        s += '.';
    }
    while (s.length <= rs + 2) {
        s += '0';
    }
    return s;
}

(function($){

    //获取地址参数
    $.getUrlParam = function(name){
        var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r!=null)
            return unescape(r[2]); return null;
    }

    //查询数组具有指定属性的元素集合
    $.where = function (arr,obj) {
           var attrs = [];
           for(i in obj){
                attrs.push({k:i,v:obj[i]});
           }
           var match = function(obj,attrs){
               for(v of attrs){
                    let key = v.k;
                    if(v.v !== obj[key] || !(key in obj)) return false;
               }
               return true;
           }
        return jQuery.grep(arr, function (v, i){ return match(v,attrs); });
    }
})(jQuery);