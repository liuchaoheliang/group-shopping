//主机服务地址
var $base = "http://localhost:8880/frontend/api/";

// 常量信息
var $cookie_uid_key = 'shop_user_id';
var $cookie_token_key = 'shop_user_token';
var $cookie_u_info_key = 'shop_user_info';

//API地址访问列表
var register_api = $base + "user/register";
var user_info_api = $base + "user/userInfo";
var user_address_api = $base + "user/address";
var save_user_address_api = $base + "user/saveAddress";
var modify_user_pwd_api = $base + "user/modifyPwd";



var merchant_apply_api = $base + "merchantApply";
var login_api = $base + "login";
var logout_api = $base + 'loginout';

//商品相关接口
var query_product_category_by_cond_api = $base + "productCategory/list";
var query_product_by_page = $base + "product/page";
var query_product_by_id = $base + "product/detail";

//订单相关
var create_order_api = $base + "order/create";
var get_order_by_page = $base + "order/page";
var get_order_status_api = $base + "order/status";

//页面相对路径
var $main_index_location = '/frontend/web/index.html';
var $login_index_location = '/frontend/login.html';


// 登录态获判断
function tokenVerify() {
    var curPath = window.location.pathname;
    var uid = $.cookie($cookie_uid_key);
    var token = $.cookie($cookie_token_key);
    if (!uid || uid == 'null') {
        if (curPath != $login_index_location) {
            // 能判断当前未登录 未登录重定向到登录页面
            // $alert("请先登录");
            // window.parent.location = $login_index_location;
        }
    } else {
        var userInfo = getUserInfo();
        $("#user-login-id").html(userInfo.loginId + "(" + userInfo.nickName + ")");
        $("#customer").hide();
        $("#user-info").show();
    }
}



function getUserInfo() {
    var userInfoJsonStr = $.cookie($cookie_u_info_key);
    if (!userInfoJsonStr || userInfoJsonStr == 'null') {
        logout();
    }
    return JSON.parse(userInfoJsonStr);
}

/*
登出动作
 */
function logout() {
    ajax(logout_api, 'get', function (data) {
        deleteTokenCookies();
        window.parent.location = $main_index_location;
    })
}

/*
* ajax封装方法 url  postData succCallback errorCallback type (默认post)  dataType(默认json)
* url <必须>
* succCallback <必须> 不然函数就没有意义
* type default post
*/
function ajax(url, type, reqJsonObj, succCallback, errorCallback) {

    if (typeof type == 'function') { // 未传入type&reqJsonObj
        succCallback = type;
        errorCallback = reqJsonObj;
        reqJsonObj = null;
    } else if (typeof type == 'object') { // 未传入type 传入reqJsonObj
        errorCallback = succCallback;
        succCallback = reqJsonObj;
        reqJsonObj = type;
        type = null;
    } else { // typeof type = 'string' 代表type传入了 可能存在reqJsonObj没有传入
        if (typeof reqJsonObj == 'function') { // reqJsonObj没有传入
            errorCallback = succCallback;
            succCallback = reqJsonObj;
            reqJsonObj = null;
        }
    }
    type = type || 'post';
    var reqJson = reqJsonObj ? JSON.stringify(reqJsonObj) : null;
    $.ajax({
        type: type,
        url: url,
        data: reqJson,
        dataType: 'json',
        contentType: 'application/json; charset=UTF-8',
        beforeSend: function () {//开始loading

        },
        success: function (result) {
            console.log(JSON.stringify(result));
            if (result.code == 200) {
                if (succCallback) {
                    succCallback(result.data);
                }
            }else {
                if(result.code == 998){
                    alert(result.msg);
                    window.parent.location = $main_index_location;
                }else{
                    if (errorCallback) {
                        errorCallback(result);
                    }
                }
            }
        },
        error: function (error) {
            alert("服务繁忙请稍后再试...");
            console.log(JSON.stringify(error));
        },
        complete: function () {//结束loading

        }
    });
}

function deleteTokenCookies() {
    $.cookie($cookie_u_info_key, null, {path: '/'});
    $.cookie($cookie_token_key, null, {path: '/'});
    $.cookie($cookie_uid_key, null, {path: '/'});
}

//获取事件触发控件
function getEventElem(event) {
    return event.srcElement || event.target;
}

function $alert(messager,title){
    //toastr.info((title || "提示"),messager);
    alert(messager);
}

/**
 * 获取日期对象
 */
function getDate(d) {
    var rd = null;
    if (d != null) {
        if (typeof d == 'object') {
            rd = d;
        } else if (typeof d == 'number') {
            rd = new Date(d);
        } else if (typeof d == 'string') {
            rd = new Date(Date.parse(d.replace(/-/g, "/")));
        }
    } else {
        rd = new Date();
    }
    var time = {};
    time.year = rd.getFullYear();
    time.month = rd.getMonth() + 1;
    time.date = rd.getDate();
    time.day = rd.getDay();
    time.hour = rd.getHours();
    time.minute = rd.getMinutes();
    time.second = rd.getSeconds();
    time.millisecond = rd.getMilliseconds();
    time.longTime = rd.getTime();

    time.年 = time.year;
    time.月 = time.month < 10 ? '0' + time.month : time.month;
    time.日 = time.date < 10 ? '0' + time.date : time.date;
    time.星期 = ['日', '一', '二', '三', '四', '五', '六'][time.day];
    time.weekDay = ['1', '2', '3', '4', '5', '6', '7'][time.day];
    time.时 = time.hour < 10 ? '0' + time.hour : time.hour;
    time.分 = time.minute < 10 ? '0' + time.minute : time.minute;
    time.秒 = time.second < 10 ? '0' + time.second : time.second;
    time.毫秒 = time.millisecond < 10 ? '00' + time.millisecond : (time.millisecond < 100 ? '0' + time.millisecond : time.millisecond);

    time.时间信息0 = time.年 + "-" + time.月 + "-" + time.日 + " " + time.时 + ":" + time.分 + ":" + time.秒 + "." + time.毫秒;
    time.时间信息1 = time.年 + "-" + time.月 + "-" + time.日 + " " + time.时 + ":" + time.分 + ":" + time.秒;
    time.时间信息2 = time.年 + "-" + time.月 + "-" + time.日;
    time.时间信息3 = time.时 + ":" + time.分 + ":" + time.秒;
    return time;
}


//强制保留2位小数，如：2，会在2后面补上00.即2.00
function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return false;
    }
    var f = Math.round(x*100)/100;
    var s = f.toString();
    var rs = s.indexOf('.');
    if (rs < 0) {
        rs = s.length;
        s += '.';
    }
    while (s.length <= rs + 2) {
        s += '0';
    }
    return s;
}

(function($){
    $.getUrlParam = function(name){
        var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r!=null)
            return unescape(r[2]); return null;
    }
})(jQuery);