/*
 * Copyright (C) 2019 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.springboot.demo;

import java.util.List;

public class PageResult {

    // 总记录数量
    private Integer totals;

    private Integer pageNum;

    private Integer pagSize;

    // 当前页数据列表
    private List<?> list;

    public PageResult() {
    }

    public PageResult(Integer totals, List<?> list) {
        this.totals = totals;
        this.list = list;
    }

    public Integer getTotals() {
        return totals;
    }

    public void setTotals(Integer totals) {
        this.totals = totals;
    }

    public List<?> getList() {
        return list;
    }

    public void setList(List<?> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "PageResult{" + "totals=" + totals + ", pageNum=" + pageNum + ", pagSize=" + pagSize + ", list=" + list
                + '}';
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPagSize() {
        return pagSize;
    }

    public void setPagSize(Integer pagSize) {
        this.pagSize = pagSize;
    }
}
