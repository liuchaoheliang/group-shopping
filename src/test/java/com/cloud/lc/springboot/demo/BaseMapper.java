/*
 * Copyright (C) 2019 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.springboot.demo;

import java.util.List;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2019/9/7
 * @since 1.0.0
 */

public interface BaseMapper<T, S> {

    int deleteByPrimaryKey(Integer id);

    int insert(T t);

    int insertSelective(T t);

    List<T> selectByExample(S s);

    T selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(T t);

    int updateByPrimaryKey(T t);
}