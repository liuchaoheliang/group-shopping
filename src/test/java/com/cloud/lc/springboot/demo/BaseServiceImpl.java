/*
 * Copyright (C) 2019 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.springboot.demo;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.cloud.lc.group.shopping.util.SpringBootBeanUtil;

/**
 * TODO: 请添加描述
 *
 * @author liuchao066
 * @date 2019/9/7
 * @since 1.0.0
 */

@Service
public class BaseServiceImpl<Pojo, PojoExample, Mapper> implements BaseService<Pojo, PojoExample> {

    /**
     * 获取当前的实例的Mapper接口
     * 
     * @return
     */
    protected Mapper getMapper() {
        ApplicationContext applicationContext = SpringBootBeanUtil.getApplicationContext();
        String className = "";
        try {
            Type[] parameterizedType =
                    ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments();
            if (parameterizedType.length == 0) {
                throw new RuntimeException("not exist parameterizedType");
            }
            className = parameterizedType[2].getTypeName();
            Class clazz = Class.forName(className);
            return applicationContext.getBean((Class<Mapper>) clazz);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 转换成baseMapper接口
     * 
     * @return
     */
    private BaseMapper<Pojo, PojoExample> getBaseMapper() {
        return (BaseMapper<Pojo, PojoExample>) this.getMapper();
    }

    @Override
    public Integer insert(Pojo t) {
        return getBaseMapper().insert(t);
    }

    @Override
    public Integer insertSelective(Pojo t) {
        return getBaseMapper().insertSelective(t);
    }

    @Override
    public Pojo getById(Integer id) {
        return getBaseMapper().selectByPrimaryKey(id);
    }

    @Override
    public void deleteById(Integer id) {
        getBaseMapper().deleteByPrimaryKey(id.intValue());
    }

    @Override
    public void updateById(Pojo t) {
        getBaseMapper().updateByPrimaryKey(t);
    }

    @Override
    public void updateByIdSelective(Pojo t) {
        getBaseMapper().updateByPrimaryKeySelective(t);
    }

    @Override
    public List<Pojo> getByConditions(PojoExample s) {
        return getBaseMapper().selectByExample(s);
    }

    @Override
    public PageResult getByPage(PojoExample s, int page, int limit) {
        PageResult pageResult = new PageResult();
        PageHelper.startPage(page, limit);
        List<Pojo> urList = getBaseMapper().selectByExample(s);
        // 获取分页查询后的数据
        PageInfo<Pojo> pageInfo = new PageInfo<Pojo>(urList);
        // 设置获取到的总记录数total：
        pageResult.setTotals(Long.valueOf(pageInfo.getTotal()).intValue());
        pageResult.setList(urList);
        pageResult.setPageNum(page);
        pageResult.setPagSize(limit);
        return pageResult;
    }
}