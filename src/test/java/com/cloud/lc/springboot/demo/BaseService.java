/*
 * Copyright (C) 2019 Baidu, Inc. All Rights Reserved.
 */
package com.cloud.lc.springboot.demo;

import java.util.List;

/**
 * 基础 service 接口类 Pojo, PojoSimple, Mapper (基于自动生成的文件)
 *
 * @author liuchao066
 * @date 2018/9/10
 * @since 1.0.0
 */
public interface BaseService<Pojo, PojoSimple> {

    public Integer insert(Pojo t);

    public Integer insertSelective(Pojo t);

    public Pojo getById(Integer id);

    public void deleteById(Integer id);

    public void updateById(Pojo t);

    public void updateByIdSelective(Pojo t);

    List<Pojo> getByConditions(PojoSimple s);

    PageResult getByPage(PojoSimple s, int pageNum, int limitSize);

}
